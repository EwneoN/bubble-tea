### This project has been written using .NET 5 and is broken up in to the following applications:
- API
  - This is an API that any application can talk to. For now that is just the Vue app.
  - This uses an SQL server database for data persistence. This database is shared with the Desktop application
- Web
  - This is a Vue.js application that consumes the endpoints exposed by the API
- Desktop
  - This is a winforms desktop client.
  - This uses an SQL server database for data persistence. This database is shared with the API application

### Database setup:
- This project uses Entity Framework(EF) and EF migrations for managing database changes
- The current connection string should work on any machine with Visual Studio installed.
- Any connection string can be used. These will need to be set in the appsettings.json files (appsettings.Development.json in the API)
- To prep the database:
  - Open the solution in VS and build the solution, ensure all nuget packages are restored
  - Set the API as the start up project
  - Open the package manager console in VS and set the Core project as the default project
  - Run the following command in the package manager console: Update-Database

### Web setup:
- Open a console window and change directory to the vue project directory: "~\BubbleTea\BubbleTea.Web\bubble-tea"
- Run npm -i to install all the required npm packages

### Running the Web application:
- Start the API application in VS
- Open a console window and change directory to the vue project directory: "~\BubbleTea\BubbleTea.Web\bubble-tea"
- Run the following command: npm run serve

## If you have any issues with the Web application not talking to the API, double check that port numbers in your API project's Debug settings match the port numbers in the App.vue file in the Web project. 
