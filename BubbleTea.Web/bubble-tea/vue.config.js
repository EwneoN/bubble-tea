module.exports = {
  productionSourceMap: false,
	filenameHashing: true,
	runtimeCompiler: process.env.NODE_ENV !== "production",
  chainWebpack: config => {
		if (process.env.NODE_ENV === "production") {
			config.plugins.delete('html');
			config.plugins.delete('preload');
			config.plugins.delete('prefetch');
		}
	},
	configureWebpack: config => {
		config.devtool = 'source-map';
	}
};