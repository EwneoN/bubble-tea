using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using BubbleTea.Core.Persistence;
using BubbleTea.Core.Repos.BaseTeas;
using BubbleTea.Core.Repos.Customers;
using BubbleTea.Core.Repos.Flavours;
using BubbleTea.Core.Repos.Toppings;
using BubbleTea.Core.Services.Ids;
using BubbleTea.Core.Services.Time;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace BubbleTea.Desktop
{
	static class Program
	{
		/// <summary>
		///  The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.SetHighDpiMode(HighDpiMode.SystemAware);
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);


			IConfiguration appSettings = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
				.AddEnvironmentVariables()
				.Build();

			var builder = new HostBuilder()
				.ConfigureServices((hostContext, services) =>
				{
					services
						.AddSingleton<MainForm>()
						.AddSingleton<IIdsService, IdsService>()
						.AddSingleton<ITimeService, TimeService>()
						.AddDbContext<BubbleTeaDbContext>(options =>
							options.UseSqlServer(appSettings.GetConnectionString("bubbletea")), ServiceLifetime.Transient)
						.AddTransient<IBaseTeasRepo, BaseTeasRepo>()
						.AddTransient<IFlavoursRepo, FlavoursRepo>()
						.AddTransient<IToppingsRepo, ToppingsRepo>()
						.AddTransient<ICustomersRepo, CustomersRepo>();
				});

			var host = builder.Build();

			using (var serviceScope = host.Services.CreateScope())
			{
				var services = serviceScope.ServiceProvider;

				try
				{
					var mainForm = services.GetService<MainForm>();

					var loadDataTask = Task.Run(async () => await mainForm.LoadData());

					loadDataTask.Wait();

					mainForm.SetData();

					Application.Run(mainForm);
				}
				catch (Exception e)
				{
					Console.WriteLine(e);
				}
			}
		}
	}
}
