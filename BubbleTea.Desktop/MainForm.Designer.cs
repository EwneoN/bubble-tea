﻿
namespace BubbleTea.Desktop
{
	partial class MainForm
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.editorTabs = new System.Windows.Forms.TabControl();
			this.bubbleTeaEditorTab = new System.Windows.Forms.TabPage();
			this.customerNameTxt = new System.Windows.Forms.TextBox();
			this.customerNameLabel = new System.Windows.Forms.Label();
			this.customersLabel = new System.Windows.Forms.Label();
			this.customersListBox = new System.Windows.Forms.ListBox();
			this.customersTabPage = new System.Windows.Forms.TabPage();
			this.toppingSaveBtn = new System.Windows.Forms.Button();
			this.toppingPriceLabel = new System.Windows.Forms.Label();
			this.toppingPriceNum = new System.Windows.Forms.NumericUpDown();
			this.toppingNameTxt = new System.Windows.Forms.TextBox();
			this.toppingNameLabel = new System.Windows.Forms.Label();
			this.flavourSaveBtn = new System.Windows.Forms.Button();
			this.flavourNameTxt = new System.Windows.Forms.TextBox();
			this.flavourNameLabel = new System.Windows.Forms.Label();
			this.baseTeaSaveBtn = new System.Windows.Forms.Button();
			this.baseTeaPriceNum = new System.Windows.Forms.NumericUpDown();
			this.baseTeaPriceLabel = new System.Windows.Forms.Label();
			this.baseTeaNameLabel = new System.Windows.Forms.Label();
			this.baseTeaNameTxt = new System.Windows.Forms.TextBox();
			this.toppingCreateBtn = new System.Windows.Forms.Button();
			this.toppingDeleteBtn = new System.Windows.Forms.Button();
			this.toppingEditBtn = new System.Windows.Forms.Button();
			this.flavourCreateBtn = new System.Windows.Forms.Button();
			this.flavourDeleteBtn = new System.Windows.Forms.Button();
			this.flavourEditBtn = new System.Windows.Forms.Button();
			this.baseTeaCreateBtn = new System.Windows.Forms.Button();
			this.deleteBaseTeaBtn = new System.Windows.Forms.Button();
			this.editBaseTeaBtn = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.toppingsListBox = new System.Windows.Forms.ListBox();
			this.flavoursLabel = new System.Windows.Forms.Label();
			this.flavoursListBox = new System.Windows.Forms.ListBox();
			this.baseTeasLabel = new System.Windows.Forms.Label();
			this.baseTeasListBox = new System.Windows.Forms.ListBox();
			this.customersEditBtn = new System.Windows.Forms.Button();
			this.customerDeleteBtn = new System.Windows.Forms.Button();
			this.customerCreateBtn = new System.Windows.Forms.Button();
			this.customerDoBLabel = new System.Windows.Forms.Label();
			this.customerDoB = new System.Windows.Forms.MonthCalendar();
			this.customerSaveBtn = new System.Windows.Forms.Button();
			this.editorTabs.SuspendLayout();
			this.bubbleTeaEditorTab.SuspendLayout();
			this.customersTabPage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.toppingPriceNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.baseTeaPriceNum)).BeginInit();
			this.SuspendLayout();
			// 
			// editorTabs
			// 
			this.editorTabs.Controls.Add(this.bubbleTeaEditorTab);
			this.editorTabs.Controls.Add(this.customersTabPage);
			this.editorTabs.Location = new System.Drawing.Point(13, 13);
			this.editorTabs.Name = "editorTabs";
			this.editorTabs.SelectedIndex = 0;
			this.editorTabs.Size = new System.Drawing.Size(844, 726);
			this.editorTabs.TabIndex = 0;
			// 
			// bubbleTeaEditorTab
			// 
			this.bubbleTeaEditorTab.Controls.Add(this.customerSaveBtn);
			this.bubbleTeaEditorTab.Controls.Add(this.customerDoB);
			this.bubbleTeaEditorTab.Controls.Add(this.customerDoBLabel);
			this.bubbleTeaEditorTab.Controls.Add(this.customerCreateBtn);
			this.bubbleTeaEditorTab.Controls.Add(this.customerDeleteBtn);
			this.bubbleTeaEditorTab.Controls.Add(this.customersEditBtn);
			this.bubbleTeaEditorTab.Controls.Add(this.customerNameTxt);
			this.bubbleTeaEditorTab.Controls.Add(this.customerNameLabel);
			this.bubbleTeaEditorTab.Controls.Add(this.customersLabel);
			this.bubbleTeaEditorTab.Controls.Add(this.customersListBox);
			this.bubbleTeaEditorTab.Location = new System.Drawing.Point(4, 24);
			this.bubbleTeaEditorTab.Name = "bubbleTeaEditorTab";
			this.bubbleTeaEditorTab.Padding = new System.Windows.Forms.Padding(3);
			this.bubbleTeaEditorTab.Size = new System.Drawing.Size(836, 698);
			this.bubbleTeaEditorTab.TabIndex = 0;
			this.bubbleTeaEditorTab.Text = "Bubble Tea Editor";
			this.bubbleTeaEditorTab.UseVisualStyleBackColor = true;
			// 
			// customerNameTxt
			// 
			this.customerNameTxt.Enabled = false;
			this.customerNameTxt.Location = new System.Drawing.Point(346, 49);
			this.customerNameTxt.Name = "customerNameTxt";
			this.customerNameTxt.Size = new System.Drawing.Size(274, 23);
			this.customerNameTxt.TabIndex = 3;
			// 
			// customerNameLabel
			// 
			this.customerNameLabel.AutoSize = true;
			this.customerNameLabel.Location = new System.Drawing.Point(346, 30);
			this.customerNameLabel.Name = "customerNameLabel";
			this.customerNameLabel.Size = new System.Drawing.Size(39, 15);
			this.customerNameLabel.TabIndex = 2;
			this.customerNameLabel.Text = "Name";
			// 
			// customersLabel
			// 
			this.customersLabel.AutoSize = true;
			this.customersLabel.Location = new System.Drawing.Point(14, 9);
			this.customersLabel.Name = "customersLabel";
			this.customersLabel.Size = new System.Drawing.Size(64, 15);
			this.customersLabel.TabIndex = 1;
			this.customersLabel.Text = "Customers";
			// 
			// customersListBox
			// 
			this.customersListBox.DisplayMember = "Name";
			this.customersListBox.FormattingEnabled = true;
			this.customersListBox.ItemHeight = 15;
			this.customersListBox.Location = new System.Drawing.Point(14, 30);
			this.customersListBox.Name = "customersListBox";
			this.customersListBox.Size = new System.Drawing.Size(239, 259);
			this.customersListBox.TabIndex = 0;
			this.customersListBox.SelectedIndexChanged += new System.EventHandler(this.customersListBox_SelectedIndexChanged);
			// 
			// customersTabPage
			// 
			this.customersTabPage.Controls.Add(this.toppingSaveBtn);
			this.customersTabPage.Controls.Add(this.toppingPriceLabel);
			this.customersTabPage.Controls.Add(this.toppingPriceNum);
			this.customersTabPage.Controls.Add(this.toppingNameTxt);
			this.customersTabPage.Controls.Add(this.toppingNameLabel);
			this.customersTabPage.Controls.Add(this.flavourSaveBtn);
			this.customersTabPage.Controls.Add(this.flavourNameTxt);
			this.customersTabPage.Controls.Add(this.flavourNameLabel);
			this.customersTabPage.Controls.Add(this.baseTeaSaveBtn);
			this.customersTabPage.Controls.Add(this.baseTeaPriceNum);
			this.customersTabPage.Controls.Add(this.baseTeaPriceLabel);
			this.customersTabPage.Controls.Add(this.baseTeaNameLabel);
			this.customersTabPage.Controls.Add(this.baseTeaNameTxt);
			this.customersTabPage.Controls.Add(this.toppingCreateBtn);
			this.customersTabPage.Controls.Add(this.toppingDeleteBtn);
			this.customersTabPage.Controls.Add(this.toppingEditBtn);
			this.customersTabPage.Controls.Add(this.flavourCreateBtn);
			this.customersTabPage.Controls.Add(this.flavourDeleteBtn);
			this.customersTabPage.Controls.Add(this.flavourEditBtn);
			this.customersTabPage.Controls.Add(this.baseTeaCreateBtn);
			this.customersTabPage.Controls.Add(this.deleteBaseTeaBtn);
			this.customersTabPage.Controls.Add(this.editBaseTeaBtn);
			this.customersTabPage.Controls.Add(this.label1);
			this.customersTabPage.Controls.Add(this.toppingsListBox);
			this.customersTabPage.Controls.Add(this.flavoursLabel);
			this.customersTabPage.Controls.Add(this.flavoursListBox);
			this.customersTabPage.Controls.Add(this.baseTeasLabel);
			this.customersTabPage.Controls.Add(this.baseTeasListBox);
			this.customersTabPage.Location = new System.Drawing.Point(4, 24);
			this.customersTabPage.Name = "customersTabPage";
			this.customersTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.customersTabPage.Size = new System.Drawing.Size(836, 698);
			this.customersTabPage.TabIndex = 1;
			this.customersTabPage.Text = "Customers";
			this.customersTabPage.UseVisualStyleBackColor = true;
			// 
			// toppingSaveBtn
			// 
			this.toppingSaveBtn.Enabled = false;
			this.toppingSaveBtn.Location = new System.Drawing.Point(549, 620);
			this.toppingSaveBtn.Name = "toppingSaveBtn";
			this.toppingSaveBtn.Size = new System.Drawing.Size(75, 23);
			this.toppingSaveBtn.TabIndex = 27;
			this.toppingSaveBtn.Text = "Save";
			this.toppingSaveBtn.UseVisualStyleBackColor = true;
			this.toppingSaveBtn.Click += new System.EventHandler(this.toppingSaveBtn_Click);
			// 
			// toppingPriceLabel
			// 
			this.toppingPriceLabel.AutoSize = true;
			this.toppingPriceLabel.Location = new System.Drawing.Point(549, 571);
			this.toppingPriceLabel.Name = "toppingPriceLabel";
			this.toppingPriceLabel.Size = new System.Drawing.Size(33, 15);
			this.toppingPriceLabel.TabIndex = 26;
			this.toppingPriceLabel.Text = "Price";
			// 
			// toppingPriceNum
			// 
			this.toppingPriceNum.DecimalPlaces = 2;
			this.toppingPriceNum.Enabled = false;
			this.toppingPriceNum.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
			this.toppingPriceNum.Location = new System.Drawing.Point(549, 590);
			this.toppingPriceNum.Name = "toppingPriceNum";
			this.toppingPriceNum.Size = new System.Drawing.Size(237, 23);
			this.toppingPriceNum.TabIndex = 25;
			// 
			// toppingNameTxt
			// 
			this.toppingNameTxt.Enabled = false;
			this.toppingNameTxt.Location = new System.Drawing.Point(549, 545);
			this.toppingNameTxt.Name = "toppingNameTxt";
			this.toppingNameTxt.Size = new System.Drawing.Size(237, 23);
			this.toppingNameTxt.TabIndex = 24;
			// 
			// toppingNameLabel
			// 
			this.toppingNameLabel.AutoSize = true;
			this.toppingNameLabel.Location = new System.Drawing.Point(549, 524);
			this.toppingNameLabel.Name = "toppingNameLabel";
			this.toppingNameLabel.Size = new System.Drawing.Size(39, 15);
			this.toppingNameLabel.TabIndex = 23;
			this.toppingNameLabel.Text = "Name";
			// 
			// flavourSaveBtn
			// 
			this.flavourSaveBtn.Enabled = false;
			this.flavourSaveBtn.Location = new System.Drawing.Point(294, 620);
			this.flavourSaveBtn.Name = "flavourSaveBtn";
			this.flavourSaveBtn.Size = new System.Drawing.Size(75, 23);
			this.flavourSaveBtn.TabIndex = 22;
			this.flavourSaveBtn.Text = "Save";
			this.flavourSaveBtn.UseVisualStyleBackColor = true;
			this.flavourSaveBtn.Click += new System.EventHandler(this.flavourSaveBtn_Click);
			// 
			// flavourNameTxt
			// 
			this.flavourNameTxt.Enabled = false;
			this.flavourNameTxt.Location = new System.Drawing.Point(294, 545);
			this.flavourNameTxt.Name = "flavourNameTxt";
			this.flavourNameTxt.Size = new System.Drawing.Size(239, 23);
			this.flavourNameTxt.TabIndex = 21;
			// 
			// flavourNameLabel
			// 
			this.flavourNameLabel.AutoSize = true;
			this.flavourNameLabel.Location = new System.Drawing.Point(294, 523);
			this.flavourNameLabel.Name = "flavourNameLabel";
			this.flavourNameLabel.Size = new System.Drawing.Size(46, 15);
			this.flavourNameLabel.TabIndex = 20;
			this.flavourNameLabel.Text = "Flavour";
			// 
			// baseTeaSaveBtn
			// 
			this.baseTeaSaveBtn.Enabled = false;
			this.baseTeaSaveBtn.Location = new System.Drawing.Point(39, 620);
			this.baseTeaSaveBtn.Name = "baseTeaSaveBtn";
			this.baseTeaSaveBtn.Size = new System.Drawing.Size(75, 23);
			this.baseTeaSaveBtn.TabIndex = 19;
			this.baseTeaSaveBtn.Text = "Save";
			this.baseTeaSaveBtn.UseVisualStyleBackColor = true;
			this.baseTeaSaveBtn.Click += new System.EventHandler(this.baseTeaSaveBtn_Click);
			// 
			// baseTeaPriceNum
			// 
			this.baseTeaPriceNum.DecimalPlaces = 2;
			this.baseTeaPriceNum.Enabled = false;
			this.baseTeaPriceNum.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
			this.baseTeaPriceNum.Location = new System.Drawing.Point(39, 590);
			this.baseTeaPriceNum.Name = "baseTeaPriceNum";
			this.baseTeaPriceNum.Size = new System.Drawing.Size(238, 23);
			this.baseTeaPriceNum.TabIndex = 18;
			// 
			// baseTeaPriceLabel
			// 
			this.baseTeaPriceLabel.AutoSize = true;
			this.baseTeaPriceLabel.Location = new System.Drawing.Point(39, 571);
			this.baseTeaPriceLabel.Name = "baseTeaPriceLabel";
			this.baseTeaPriceLabel.Size = new System.Drawing.Size(33, 15);
			this.baseTeaPriceLabel.TabIndex = 17;
			this.baseTeaPriceLabel.Text = "Price";
			// 
			// baseTeaNameLabel
			// 
			this.baseTeaNameLabel.AutoSize = true;
			this.baseTeaNameLabel.Location = new System.Drawing.Point(39, 524);
			this.baseTeaNameLabel.Name = "baseTeaNameLabel";
			this.baseTeaNameLabel.Size = new System.Drawing.Size(39, 15);
			this.baseTeaNameLabel.TabIndex = 16;
			this.baseTeaNameLabel.Text = "Name";
			// 
			// baseTeaNameTxt
			// 
			this.baseTeaNameTxt.Enabled = false;
			this.baseTeaNameTxt.Location = new System.Drawing.Point(39, 545);
			this.baseTeaNameTxt.Name = "baseTeaNameTxt";
			this.baseTeaNameTxt.Size = new System.Drawing.Size(238, 23);
			this.baseTeaNameTxt.TabIndex = 15;
			// 
			// toppingCreateBtn
			// 
			this.toppingCreateBtn.Location = new System.Drawing.Point(711, 484);
			this.toppingCreateBtn.Name = "toppingCreateBtn";
			this.toppingCreateBtn.Size = new System.Drawing.Size(75, 23);
			this.toppingCreateBtn.TabIndex = 14;
			this.toppingCreateBtn.Text = "Create";
			this.toppingCreateBtn.UseVisualStyleBackColor = true;
			this.toppingCreateBtn.Click += new System.EventHandler(this.toppingCreateBtn_Click);
			// 
			// toppingDeleteBtn
			// 
			this.toppingDeleteBtn.Location = new System.Drawing.Point(630, 484);
			this.toppingDeleteBtn.Name = "toppingDeleteBtn";
			this.toppingDeleteBtn.Size = new System.Drawing.Size(75, 23);
			this.toppingDeleteBtn.TabIndex = 13;
			this.toppingDeleteBtn.Text = "Delete";
			this.toppingDeleteBtn.UseVisualStyleBackColor = true;
			this.toppingDeleteBtn.Click += new System.EventHandler(this.toppingDeleteBtn_Click);
			// 
			// toppingEditBtn
			// 
			this.toppingEditBtn.Location = new System.Drawing.Point(549, 484);
			this.toppingEditBtn.Name = "toppingEditBtn";
			this.toppingEditBtn.Size = new System.Drawing.Size(75, 23);
			this.toppingEditBtn.TabIndex = 12;
			this.toppingEditBtn.Text = "Edit";
			this.toppingEditBtn.UseVisualStyleBackColor = true;
			this.toppingEditBtn.Click += new System.EventHandler(this.toppingEditBtn_Click);
			// 
			// flavourCreateBtn
			// 
			this.flavourCreateBtn.Location = new System.Drawing.Point(458, 484);
			this.flavourCreateBtn.Name = "flavourCreateBtn";
			this.flavourCreateBtn.Size = new System.Drawing.Size(75, 23);
			this.flavourCreateBtn.TabIndex = 11;
			this.flavourCreateBtn.Text = "Create";
			this.flavourCreateBtn.UseVisualStyleBackColor = true;
			this.flavourCreateBtn.Click += new System.EventHandler(this.flavourCreateBtn_Click);
			// 
			// flavourDeleteBtn
			// 
			this.flavourDeleteBtn.Location = new System.Drawing.Point(376, 484);
			this.flavourDeleteBtn.Name = "flavourDeleteBtn";
			this.flavourDeleteBtn.Size = new System.Drawing.Size(75, 23);
			this.flavourDeleteBtn.TabIndex = 10;
			this.flavourDeleteBtn.Text = "Delete";
			this.flavourDeleteBtn.UseVisualStyleBackColor = true;
			this.flavourDeleteBtn.Click += new System.EventHandler(this.flavourDeleteBtn_Click);
			// 
			// flavourEditBtn
			// 
			this.flavourEditBtn.Location = new System.Drawing.Point(294, 484);
			this.flavourEditBtn.Name = "flavourEditBtn";
			this.flavourEditBtn.Size = new System.Drawing.Size(75, 23);
			this.flavourEditBtn.TabIndex = 9;
			this.flavourEditBtn.Text = "Edit";
			this.flavourEditBtn.UseVisualStyleBackColor = true;
			this.flavourEditBtn.Click += new System.EventHandler(this.flavourEditBtn_Click);
			// 
			// baseTeaCreateBtn
			// 
			this.baseTeaCreateBtn.Location = new System.Drawing.Point(202, 484);
			this.baseTeaCreateBtn.Name = "baseTeaCreateBtn";
			this.baseTeaCreateBtn.Size = new System.Drawing.Size(75, 23);
			this.baseTeaCreateBtn.TabIndex = 8;
			this.baseTeaCreateBtn.Text = "Create";
			this.baseTeaCreateBtn.UseVisualStyleBackColor = true;
			this.baseTeaCreateBtn.Click += new System.EventHandler(this.baseTeaCreateBtn_Click);
			// 
			// deleteBaseTeaBtn
			// 
			this.deleteBaseTeaBtn.Location = new System.Drawing.Point(121, 484);
			this.deleteBaseTeaBtn.Name = "deleteBaseTeaBtn";
			this.deleteBaseTeaBtn.Size = new System.Drawing.Size(75, 23);
			this.deleteBaseTeaBtn.TabIndex = 7;
			this.deleteBaseTeaBtn.Text = "Delete";
			this.deleteBaseTeaBtn.UseVisualStyleBackColor = true;
			this.deleteBaseTeaBtn.Click += new System.EventHandler(this.deleteBaseTeaBtn_Click);
			// 
			// editBaseTeaBtn
			// 
			this.editBaseTeaBtn.Location = new System.Drawing.Point(39, 484);
			this.editBaseTeaBtn.Name = "editBaseTeaBtn";
			this.editBaseTeaBtn.Size = new System.Drawing.Size(75, 23);
			this.editBaseTeaBtn.TabIndex = 6;
			this.editBaseTeaBtn.Text = "Edit";
			this.editBaseTeaBtn.UseVisualStyleBackColor = true;
			this.editBaseTeaBtn.Click += new System.EventHandler(this.editBaseTeaBtn_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(549, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(55, 15);
			this.label1.TabIndex = 5;
			this.label1.Text = "Toppings";
			// 
			// toppingsListBox
			// 
			this.toppingsListBox.DisplayMember = "Name";
			this.toppingsListBox.FormattingEnabled = true;
			this.toppingsListBox.ItemHeight = 15;
			this.toppingsListBox.Location = new System.Drawing.Point(549, 36);
			this.toppingsListBox.Name = "toppingsListBox";
			this.toppingsListBox.Size = new System.Drawing.Size(249, 439);
			this.toppingsListBox.TabIndex = 4;
			this.toppingsListBox.SelectedIndexChanged += new System.EventHandler(this.toppingsListBox_SelectedIndexChanged);
			// 
			// flavoursLabel
			// 
			this.flavoursLabel.AutoSize = true;
			this.flavoursLabel.Location = new System.Drawing.Point(294, 15);
			this.flavoursLabel.Name = "flavoursLabel";
			this.flavoursLabel.Size = new System.Drawing.Size(51, 15);
			this.flavoursLabel.TabIndex = 3;
			this.flavoursLabel.Text = "Flavours";
			// 
			// flavoursListBox
			// 
			this.flavoursListBox.DisplayMember = "Name";
			this.flavoursListBox.FormattingEnabled = true;
			this.flavoursListBox.ItemHeight = 15;
			this.flavoursListBox.Location = new System.Drawing.Point(294, 36);
			this.flavoursListBox.Name = "flavoursListBox";
			this.flavoursListBox.Size = new System.Drawing.Size(249, 439);
			this.flavoursListBox.TabIndex = 2;
			this.flavoursListBox.SelectedIndexChanged += new System.EventHandler(this.flavoursListBox_SelectedIndexChanged);
			// 
			// baseTeasLabel
			// 
			this.baseTeasLabel.AutoSize = true;
			this.baseTeasLabel.Location = new System.Drawing.Point(39, 15);
			this.baseTeasLabel.Name = "baseTeasLabel";
			this.baseTeasLabel.Size = new System.Drawing.Size(56, 15);
			this.baseTeasLabel.TabIndex = 1;
			this.baseTeasLabel.Text = "Base Teas";
			// 
			// baseTeasListBox
			// 
			this.baseTeasListBox.DisplayMember = "Name";
			this.baseTeasListBox.FormattingEnabled = true;
			this.baseTeasListBox.ItemHeight = 15;
			this.baseTeasListBox.Location = new System.Drawing.Point(39, 36);
			this.baseTeasListBox.Name = "baseTeasListBox";
			this.baseTeasListBox.Size = new System.Drawing.Size(249, 439);
			this.baseTeasListBox.TabIndex = 0;
			this.baseTeasListBox.SelectedIndexChanged += new System.EventHandler(this.baseTeasListBox_SelectedIndexChanged);
			// 
			// customersEditBtn
			// 
			this.customersEditBtn.Location = new System.Drawing.Point(14, 296);
			this.customersEditBtn.Name = "customersEditBtn";
			this.customersEditBtn.Size = new System.Drawing.Size(75, 23);
			this.customersEditBtn.TabIndex = 4;
			this.customersEditBtn.Text = "Edit";
			this.customersEditBtn.UseVisualStyleBackColor = true;
			this.customersEditBtn.Click += new System.EventHandler(this.customersEditBtn_Click);
			// 
			// customerDeleteBtn
			// 
			this.customerDeleteBtn.Location = new System.Drawing.Point(96, 296);
			this.customerDeleteBtn.Name = "customerDeleteBtn";
			this.customerDeleteBtn.Size = new System.Drawing.Size(75, 23);
			this.customerDeleteBtn.TabIndex = 5;
			this.customerDeleteBtn.Text = "Delete";
			this.customerDeleteBtn.UseVisualStyleBackColor = true;
			this.customerDeleteBtn.Click += new System.EventHandler(this.customerDeleteBtn_Click);
			// 
			// customerCreateBtn
			// 
			this.customerCreateBtn.Location = new System.Drawing.Point(178, 296);
			this.customerCreateBtn.Name = "customerCreateBtn";
			this.customerCreateBtn.Size = new System.Drawing.Size(75, 23);
			this.customerCreateBtn.TabIndex = 6;
			this.customerCreateBtn.Text = "Create";
			this.customerCreateBtn.UseVisualStyleBackColor = true;
			this.customerCreateBtn.Click += new System.EventHandler(this.customerCreateBtn_Click);
			// 
			// customerDoBLabel
			// 
			this.customerDoBLabel.AutoSize = true;
			this.customerDoBLabel.Location = new System.Drawing.Point(346, 79);
			this.customerDoBLabel.Name = "customerDoBLabel";
			this.customerDoBLabel.Size = new System.Drawing.Size(73, 15);
			this.customerDoBLabel.TabIndex = 7;
			this.customerDoBLabel.Text = "Date of Birth";
			// 
			// customerDoB
			// 
			this.customerDoB.Enabled = false;
			this.customerDoB.Location = new System.Drawing.Point(346, 98);
			this.customerDoB.MaxSelectionCount = 1;
			this.customerDoB.Name = "customerDoB";
			this.customerDoB.TabIndex = 8;
			// 
			// customerSaveBtn
			// 
			this.customerSaveBtn.Enabled = false;
			this.customerSaveBtn.Location = new System.Drawing.Point(346, 296);
			this.customerSaveBtn.Name = "customerSaveBtn";
			this.customerSaveBtn.Size = new System.Drawing.Size(75, 23);
			this.customerSaveBtn.TabIndex = 9;
			this.customerSaveBtn.Text = "Save";
			this.customerSaveBtn.UseVisualStyleBackColor = true;
			this.customerSaveBtn.Click += new System.EventHandler(this.customerSaveBtn_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(872, 744);
			this.Controls.Add(this.editorTabs);
			this.Name = "MainForm";
			this.Text = "Bubble Tea Thick Client";
			this.editorTabs.ResumeLayout(false);
			this.bubbleTeaEditorTab.ResumeLayout(false);
			this.bubbleTeaEditorTab.PerformLayout();
			this.customersTabPage.ResumeLayout(false);
			this.customersTabPage.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.toppingPriceNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.baseTeaPriceNum)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl editorTabs;
		private System.Windows.Forms.TabPage bubbleTeaEditorTab;
		private System.Windows.Forms.TabPage customersTabPage;
		private System.Windows.Forms.Button toppingCreateBtn;
		private System.Windows.Forms.Button toppingDeleteBtn;
		private System.Windows.Forms.Button toppingEditBtn;
		private System.Windows.Forms.Button flavourCreateBtn;
		private System.Windows.Forms.Button flavourDeleteBtn;
		private System.Windows.Forms.Button flavourEditBtn;
		private System.Windows.Forms.Button baseTeaCreateBtn;
		private System.Windows.Forms.Button deleteBaseTeaBtn;
		private System.Windows.Forms.Button editBaseTeaBtn;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ListBox toppingsListBox;
		private System.Windows.Forms.Label flavoursLabel;
		private System.Windows.Forms.ListBox flavoursListBox;
		private System.Windows.Forms.Label baseTeasLabel;
		private System.Windows.Forms.ListBox baseTeasListBox;
		private System.Windows.Forms.Button toppingSaveBtn;
		private System.Windows.Forms.Label toppingPriceLabel;
		private System.Windows.Forms.NumericUpDown toppingPriceNum;
		private System.Windows.Forms.TextBox toppingNameTxt;
		private System.Windows.Forms.Label toppingNameLabel;
		private System.Windows.Forms.Button flavourSaveBtn;
		private System.Windows.Forms.TextBox flavourNameTxt;
		private System.Windows.Forms.Label flavourNameLabel;
		private System.Windows.Forms.Button baseTeaSaveBtn;
		private System.Windows.Forms.NumericUpDown numericUpDown1;
		private System.Windows.Forms.Label baseTeaPriceLabel;
		private System.Windows.Forms.Label baseTeaNameLabel;
		private System.Windows.Forms.TextBox baseTeaNameTxt;
		private System.Windows.Forms.Label customerNameLabel;
		private System.Windows.Forms.Label customersLabel;
		private System.Windows.Forms.ListBox customersListBox;
		private System.Windows.Forms.TextBox customerNameTxt;
		private System.Windows.Forms.Button customerSaveBtn;
		private System.Windows.Forms.MonthCalendar customerDoB;
		private System.Windows.Forms.Label customerDoBLabel;
		private System.Windows.Forms.Button customerCreateBtn;
		private System.Windows.Forms.Button customerDeleteBtn;
		private System.Windows.Forms.Button customersEditBtn;
		private System.Windows.Forms.NumericUpDown baseTeaPriceNum;
	}
}

