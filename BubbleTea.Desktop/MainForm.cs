﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using BubbleTea.Core.Dtos.BaseTeas;
using BubbleTea.Core.Dtos.Customers;
using BubbleTea.Core.Dtos.Flavours;
using BubbleTea.Core.Dtos.Toppings;
using BubbleTea.Core.Models;
using BubbleTea.Core.Repos.BaseTeas;
using BubbleTea.Core.Repos.Customers;
using BubbleTea.Core.Repos.Flavours;
using BubbleTea.Core.Repos.Toppings;

namespace BubbleTea.Desktop
{
	public partial class MainForm : Form
	{
		private readonly ICustomersRepo _CustomersRepo;
		private readonly IBaseTeasRepo _BaseTeasRepo;
		private readonly IFlavoursRepo _FlavoursRepo;
		private readonly IToppingsRepo _ToppingsRepo;
		private BindingList<BaseTea> _BaseTeas;
		private BindingList<Flavour> _Flavours;
		private BindingList<Topping> _Toppings;
		private BindingList<Customer> _Customers;

		private BaseTea _SelectedBaseTea;
		private Flavour _SelectedFlavour;
		private Topping _SelectedTopping;
		private Customer _SelectedCustomer;

		public MainForm(ICustomersRepo customersRepo, IBaseTeasRepo baseTeasRepo, IFlavoursRepo flavoursRepo,
			IToppingsRepo toppingsRepo)
		{
			_CustomersRepo = customersRepo ?? throw new ArgumentNullException(nameof(customersRepo));
			_BaseTeasRepo = baseTeasRepo ?? throw new ArgumentNullException(nameof(baseTeasRepo));
			_FlavoursRepo = flavoursRepo ?? throw new ArgumentNullException(nameof(flavoursRepo));
			_ToppingsRepo = toppingsRepo ?? throw new ArgumentNullException(nameof(toppingsRepo));

			InitializeComponent();
		}

		public async Task LoadData()
		{
			_BaseTeas = new BindingList<BaseTea>((await _BaseTeasRepo.GetBaseTeas()).ToList());
			_Flavours = new BindingList<Flavour>((await _FlavoursRepo.GetFlavours()).ToList());
			_Toppings = new BindingList<Topping>((await _ToppingsRepo.GetToppings()).ToList());
			_Customers = new BindingList<Customer>((await _CustomersRepo.GetCustomers()).ToList());
		}

		public void SetData()
		{
			baseTeasListBox.DataSource = _BaseTeas;
			flavoursListBox.DataSource = _Flavours;
			toppingsListBox.DataSource = _Toppings;
			customersListBox.DataSource = _Customers;
		}

		private void baseTeasListBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			int index = (sender as ListBox)?.SelectedIndex ?? -1;

			if (index < 0)
			{
				_SelectedBaseTea = null;
				baseTeaNameTxt.Text = null;
				baseTeaPriceNum.Value = 0;
				baseTeaNameTxt.Enabled = false;
				baseTeaPriceNum.Enabled = false;
				baseTeaSaveBtn.Enabled = false;
				return;
			}

			_SelectedBaseTea = _BaseTeas.ElementAt(index);

			if (!baseTeaSaveBtn.Enabled)
			{
				return;
			}

			baseTeaNameTxt.Text = _SelectedBaseTea.Name;
			baseTeaPriceNum.Value = _SelectedBaseTea.Price;
		}

		private void editBaseTeaBtn_Click(object sender, EventArgs e)
		{
			if (_SelectedBaseTea == null)
			{
				return;
			}

			baseTeaNameTxt.Text = _SelectedBaseTea.Name;
			baseTeaPriceNum.Value = _SelectedBaseTea.Price;

			baseTeaNameTxt.Enabled = true;
			baseTeaPriceNum.Enabled = true;
			baseTeaSaveBtn.Enabled = true;
		}

		private async void deleteBaseTeaBtn_Click(object sender, EventArgs e)
		{
			if (_SelectedBaseTea == null)
			{
				return;
			}

			var result = MessageBox
				.Show($"Are you sure u want to delete {_SelectedBaseTea.Name}?", "Confirm", MessageBoxButtons.YesNo);

			if (result == DialogResult.No)
			{
				return;
			}

			_BaseTeas.Remove(_SelectedBaseTea);

			await _BaseTeasRepo.DeleteBaseTea(_SelectedBaseTea.Id);

			_SelectedBaseTea = null;
		}

		private void baseTeaCreateBtn_Click(object sender, EventArgs e)
		{
			_SelectedBaseTea = null;
			baseTeaNameTxt.Text = null;
			baseTeaPriceNum.Value = 0;

			baseTeaNameTxt.Enabled = true;
			baseTeaPriceNum.Enabled = true;
			baseTeaSaveBtn.Enabled = true;
		}

		private async void baseTeaSaveBtn_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(baseTeaNameTxt.Text))
			{
				MessageBox.Show("Base Tea Name is required");
				return;
			}

			if (baseTeaPriceNum.Value <= 0)
			{
				MessageBox.Show("Base Tea Price must be greater than 0");
				return;
			}

			if (_SelectedBaseTea != null)
			{
				_SelectedBaseTea.Name = baseTeaNameTxt.Text;
				_SelectedBaseTea.Price = baseTeaPriceNum.Value;

				BaseTeaUpdate update = new BaseTeaUpdate
				{
					Id = _SelectedBaseTea.Id,
					Name = _SelectedBaseTea.Name,
					Price = _SelectedBaseTea.Price
				};

				await _BaseTeasRepo.UpdateBaseTea(update);
			}
			else
			{
				NewBaseTea newBaseTea = new NewBaseTea
				{
					Name = baseTeaNameTxt.Text,
					Price = baseTeaPriceNum.Value
				};

				BaseTea baseTea = await _BaseTeasRepo.CreateBaseTea(newBaseTea);
				_BaseTeas.Add(baseTea);

				baseTeasListBox.Refresh();
			}

			_SelectedBaseTea = null;
			baseTeasListBox.SelectedIndex = -1;
			baseTeaNameTxt.Text = null;
			baseTeaPriceNum.Value = 0;
			baseTeaNameTxt.Enabled = false;
			baseTeaPriceNum.Enabled = false;
			baseTeaSaveBtn.Enabled = false;

			MessageBox.Show("Base Tea saved!");
		}

		private void flavoursListBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			int index = (sender as ListBox)?.SelectedIndex ?? -1;

			if (index < 0)
			{
				_SelectedFlavour = null;
				flavourNameTxt.Text = null;
				flavourNameTxt.Enabled = false;
				flavourSaveBtn.Enabled = false;
				return;
			}

			_SelectedFlavour = _Flavours.ElementAt(index);

			if (!flavourSaveBtn.Enabled)
			{
				return;
			}

			flavourNameTxt.Text = _SelectedFlavour.Name;
		}

		private void flavourEditBtn_Click(object sender, EventArgs e)
		{
			if (_SelectedFlavour == null)
			{
				return;
			}

			flavourNameTxt.Text = _SelectedFlavour.Name;

			flavourNameTxt.Enabled = true;
			flavourSaveBtn.Enabled = true;
		}

		private async void flavourDeleteBtn_Click(object sender, EventArgs e)
		{
			if (_SelectedFlavour == null)
			{
				return;
			}

			var result = MessageBox
				.Show($"Are you sure u want to delete {_SelectedFlavour.Name}?", "Confirm", MessageBoxButtons.YesNo);

			if (result == DialogResult.No)
			{
				return;
			}

			_Flavours.Remove(_SelectedFlavour);

			await _FlavoursRepo.DeleteFlavour(_SelectedFlavour.Id);

			_SelectedFlavour = null;
		}

		private void flavourCreateBtn_Click(object sender, EventArgs e)
		{
			_SelectedFlavour = null;
			flavourNameTxt.Text = null;
			flavourNameTxt.Enabled = true;
			flavourSaveBtn.Enabled = true;
		}

		private async void flavourSaveBtn_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(flavourNameTxt.Text))
			{
				MessageBox.Show("Flavour Name is required");
				return;
			}

			if (_SelectedFlavour != null)
			{
				FlavourUpdate update = new FlavourUpdate
				{
					Id = _SelectedFlavour.Id,
					Name = flavourNameTxt.Text
				};

				await _FlavoursRepo.UpdateFlavour(update);
			}
			else
			{
				NewFlavour newFlavour = new NewFlavour
				{
					Name = flavourNameTxt.Text
				};

				Flavour flavour = await _FlavoursRepo.CreateFlavour(newFlavour);
				_Flavours.Add(flavour);

				flavoursListBox.Refresh();
			}

			_SelectedFlavour = null;
			flavoursListBox.SelectedIndex = -1;
			flavourNameTxt.Text = null;
			flavourNameTxt.Enabled = false;
			flavourSaveBtn.Enabled = false;

			MessageBox.Show("Flavour saved!");
		}

		private void toppingsListBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			int index = (sender as ListBox)?.SelectedIndex ?? -1;

			if (index < 0)
			{
				_SelectedTopping = null;
				toppingNameTxt.Text = null;
				toppingPriceNum.Value = 0;
				toppingNameTxt.Enabled = false;
				toppingPriceNum.Enabled = false;
				toppingSaveBtn.Enabled = false;
				return;
			}

			_SelectedTopping = _Toppings.ElementAt(index);

			if (!toppingSaveBtn.Enabled)
			{
				return;
			}

			toppingNameTxt.Text = _SelectedTopping.Name;
			toppingPriceNum.Value = _SelectedTopping.Price;
		}
		
		private void toppingEditBtn_Click(object sender, EventArgs e)
		{
			if (_SelectedTopping == null)
			{
				return;
			}

			toppingNameTxt.Text = _SelectedTopping.Name;
			toppingPriceNum.Value = _SelectedTopping.Price;

			toppingNameTxt.Enabled = true;
			toppingPriceNum.Enabled = true;
			toppingSaveBtn.Enabled = true;
		}

		private async void toppingDeleteBtn_Click(object sender, EventArgs e)
		{
			if (_SelectedTopping == null)
			{
				return;
			}

			var result = MessageBox
				.Show($"Are you sure u want to delete {_SelectedTopping.Name}?", "Confirm", MessageBoxButtons.YesNo);

			if (result == DialogResult.No)
			{
				return;
			}

			_Toppings.Remove(_SelectedTopping);

			await _ToppingsRepo.DeleteTopping(_SelectedTopping.Id);

			_SelectedTopping = null;
		}

		private void toppingCreateBtn_Click(object sender, EventArgs e)
		{
			_SelectedTopping = null;
			toppingNameTxt.Text = null;
			toppingPriceNum.Value = 0;
			toppingNameTxt.Enabled = true;
			toppingPriceNum.Enabled = true;
			toppingSaveBtn.Enabled = true;
		}

		private async void toppingSaveBtn_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(toppingNameTxt.Text))
			{
				MessageBox.Show("Topping Name is required");
				return;
			}

			if (toppingPriceNum.Value <= 0)
			{
				MessageBox.Show("Topping Price must be greater than 0");
				return;
			}

			if (_SelectedTopping != null)
			{
				ToppingUpdate update = new ToppingUpdate
				{
					Id = _SelectedTopping.Id,
					Name = toppingNameTxt.Text,
					Price = toppingPriceNum.Value
				};

				await _ToppingsRepo.UpdateTopping(update);
			}
			else
			{
				NewTopping newTopping = new NewTopping
				{
					Name = toppingNameTxt.Text,
					Price = toppingPriceNum.Value
				};

				Topping topping = await _ToppingsRepo.CreateTopping(newTopping);
				_Toppings.Add(topping);

				toppingsListBox.Refresh();
			}

			_SelectedTopping = null;
			toppingsListBox.SelectedIndex = -1;
			toppingNameTxt.Text = null;
			toppingPriceNum.Value = 0;
			toppingNameTxt.Enabled = false;
			toppingPriceNum.Enabled = false;
			toppingSaveBtn.Enabled = false;

			MessageBox.Show("Topping saved!");
		}

		private void customersListBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			int index = (sender as ListBox)?.SelectedIndex ?? -1;

			if (index < 0)
			{
				_SelectedCustomer = null;
				customerNameTxt.Text = null;
				customerDoB.SelectionStart = DateTime.Now;
				customerDoB.SelectionEnd = DateTime.Now;
				customerNameTxt.Enabled = false;
				customerDoB.Enabled = false;
				customerSaveBtn.Enabled = false;
				return;
			}

			_SelectedCustomer = _Customers.ElementAt(index);

			if (!customerSaveBtn.Enabled)
			{
				return;
			}

			customerNameTxt.Text = _SelectedCustomer.Name;
			customerDoB.SelectionStart = _SelectedCustomer.DateOfBirth.Date;
			customerDoB.SelectionEnd = _SelectedCustomer.DateOfBirth.Date;
		}

		private void customersEditBtn_Click(object sender, EventArgs e)
		{
			if (_SelectedCustomer == null)
			{
				return;
			}

			customerNameTxt.Text = _SelectedCustomer.Name;
			customerDoB.SelectionStart = _SelectedCustomer.DateOfBirth.Date;
			customerDoB.SelectionEnd = _SelectedCustomer.DateOfBirth.Date;

			customerNameTxt.Enabled = true;
			customerDoB.Enabled = true;
			customerSaveBtn.Enabled = true;
		}

		private async void customerDeleteBtn_Click(object sender, EventArgs e)
		{
			if (_SelectedCustomer == null)
			{
				return;
			}

			var result = MessageBox
				.Show($"Are you sure u want to delete {_SelectedCustomer.Name}?", "Confirm", MessageBoxButtons.YesNo);

			if (result == DialogResult.No)
			{
				return;
			}

			_Customers.Remove(_SelectedCustomer);

			await _CustomersRepo.DeleteCustomer(_SelectedCustomer.Id);

			_SelectedCustomer = null;
		}

		private void customerCreateBtn_Click(object sender, EventArgs e)
		{
			_SelectedCustomer = null;
			customerNameTxt.Text = null;
			customerDoB.SelectionStart = DateTime.Now;
			customerDoB.SelectionEnd = DateTime.Now;

			customerNameTxt.Enabled = true;
			customerDoB.Enabled = true;
			customerSaveBtn.Enabled = true;
		}
		
		private async void customerSaveBtn_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(customerNameTxt.Text))
			{
				MessageBox.Show("Customer Name is required");
				return;
			}

			if (customerDoB.SelectionStart == DateTime.MinValue)
			{
				MessageBox.Show("Customer Date of Birth is required");
				return;
			}

			if (_SelectedCustomer != null)
			{
				CustomerUpdate update = new CustomerUpdate
				{
					Id = _SelectedCustomer.Id,
					Name = customerNameTxt.Text,
					DateOfBirth = new DateTimeOffset(customerDoB.SelectionStart, TimeSpan.FromHours(10))
				};

				await _CustomersRepo.UpdateCustomer(update);
			}
			else
			{
				NewCustomer newCustomer = new NewCustomer
				{
					Name = customerNameTxt.Text,
					DateOfBirth = new DateTimeOffset(customerDoB.SelectionStart, TimeSpan.FromHours(10))
				};

				Customer customer = await _CustomersRepo.CreateCustomer(newCustomer);
				_Customers.Add(customer);

				customersListBox.Refresh();
			}

			_SelectedCustomer = null;
			customersListBox.SelectedIndex = -1;
			customerNameTxt.Text = null;
			customerDoB.SelectionStart = DateTime.Now;
			customerDoB.SelectionEnd = DateTime.Now;
			customerNameTxt.Enabled = false;
			customerDoB.Enabled = false;
			customerSaveBtn.Enabled = false;

			MessageBox.Show("Customer saved!");
		}
	}
}