﻿using System;
using System.IO;
using BubbleTea.Core.Persistence;
using BubbleTea.Core.Services.Ids;
using BubbleTea.Core.Services.Time;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace BubbleTea.Api
{
	public class BubbleTeaDbContextDesignTimeFactory : IDesignTimeDbContextFactory<BubbleTeaDbContext>
	{
		public BubbleTeaDbContext CreateDbContext(string[] args)
		{
			string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

			IConfiguration appSettings = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
				.AddJsonFile($"appsettings.{environment}.json", optional: true)
				.AddEnvironmentVariables()
				.Build();
			
			var timeService = new TimeService();
			var idService = new IdsService();

			DbContextOptionsBuilder<BubbleTeaDbContext> builder = new DbContextOptionsBuilder<BubbleTeaDbContext>();
			builder.UseSqlServer(appSettings.GetConnectionString("bubbletea"));

			return new BubbleTeaDbContext(builder.Options, timeService, idService);
    }
	}
}
