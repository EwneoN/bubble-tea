using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BubbleTea.Core.Persistence;
using BubbleTea.Core.Repos.BaseTeas;
using BubbleTea.Core.Repos.Customers;
using BubbleTea.Core.Repos.Flavours;
using BubbleTea.Core.Repos.Orders;
using BubbleTea.Core.Repos.Sizes;
using BubbleTea.Core.Repos.Toppings;
using BubbleTea.Core.Services;
using BubbleTea.Core.Services.Ids;
using BubbleTea.Core.Services.Time;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace BubbleTea.Api
{
	public class Startup
	{
		public IConfiguration Configuration { get; }

		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddControllers();

			services
				.AddSingleton<IIdsService, IdsService>()
				.AddSingleton<ITimeService, TimeService>()
				.AddDbContext<BubbleTeaDbContext>(options => options.UseSqlServer("name=bubbletea"), ServiceLifetime.Transient)
				.AddTransient<IOrdersRepo, OrdersRepo>()
				.AddTransient<ISizesRepo, SizesRepo>()
				.AddTransient<IBaseTeasRepo, BaseTeasRepo>()
				.AddTransient<IFlavoursRepo, FlavoursRepo>()
				.AddTransient<IToppingsRepo, ToppingsRepo>()
				.AddTransient<ICustomersRepo, CustomersRepo>()
				.AddTransient<IOrderingService, OrderingService>();

			var cors = new CorsPolicyBuilder();
			cors.AllowAnyHeader();
			cors.AllowAnyMethod();
			cors.AllowAnyOrigin();

			services.AddCors(options =>
			{
				options.AddPolicy("BubbleTeaCorsPolicy", cors.Build());
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseHttpsRedirection();

			app.UseRouting();

			app.UseCors("BubbleTeaCorsPolicy");

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
