﻿using System;
using System.Threading.Tasks;
using BubbleTea.Core.Repos.Customers;
using Microsoft.AspNetCore.Mvc;

namespace BubbleTea.Api.Controllers
{
	[Route("/customers")]
	public class CustomersController : ControllerBase
	{
		//usually i'd use a service which is injected with a repo rather than a repo directly
		//but for speed im just using the repo here
		private readonly ICustomersRepo _CustomersRepo;

		public CustomersController(ICustomersRepo baseTeasRepo)
		{
			_CustomersRepo = baseTeasRepo ?? throw new ArgumentNullException(nameof(baseTeasRepo));
		}

		[Route("")]
		[HttpGet]
		public async Task<IActionResult> GetCustomers()
		{
			var customers = await _CustomersRepo.GetCustomers();

			return Ok(customers);
		}
	}
}