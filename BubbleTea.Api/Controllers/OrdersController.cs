﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BubbleTea.Core.Dtos;
using BubbleTea.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace BubbleTea.Api.Controllers
{
	[Route("/orders")]
	public class OrdersController : ControllerBase
	{
		private readonly IOrderingService _OrdersService;

		public OrdersController(IOrderingService ordersService)
		{
			_OrdersService = ordersService ?? throw new ArgumentNullException(nameof(ordersService));
		}

		[Route("{id:Guid}")]
		[HttpGet]
		public async Task<IActionResult> GetOrder(Guid id)
		{
			BubbleTeaOrder order = await _OrdersService.GetOrder(id);

			return Ok(order);
		}

		[Route("")]
		[HttpGet]
		public async Task<IActionResult> GetOrders()
		{
			ICollection<BubbleTeaOrder> orders = await _OrdersService.GetOrders();

			return Ok(orders);
		}

		[Route("")]
		[HttpPut]
		public async Task<IActionResult> CreateOrder([FromBody]NewBubbleTeaOrder newBubbleTeaOrder)
		{
			BubbleTeaOrder order = await _OrdersService.CreateOrder(newBubbleTeaOrder);

			return Ok(order);
		}

		[Route("")]
		[HttpPost]
		public async Task<IActionResult> UpdateOrder([FromBody]BubbleTeaOrderUpdate bubbleTeaOrderUpdate)
		{
			BubbleTeaOrder order = await _OrdersService.UpdateOrder(bubbleTeaOrderUpdate);

			return Ok(order);
		}

		[Route("{id:Guid}")]
		[HttpDelete]
		public async Task<IActionResult> DeleteOrder(Guid id)
		{
			await _OrdersService.DeleteOrder(id);

			return Ok();
		}
	}
}
