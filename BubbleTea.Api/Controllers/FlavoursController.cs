﻿using System;
using System.Threading.Tasks;
using BubbleTea.Core.Repos.Flavours;
using Microsoft.AspNetCore.Mvc;

namespace BubbleTea.Api.Controllers
{
	[Route("/flavours")]
	public class FlavoursController : ControllerBase
	{
		//usually i'd use a service which is injected with a repo rather than a repo directly
		//but for speed im just using the repo here
		private readonly IFlavoursRepo _FlavoursRepo;

		public FlavoursController(IFlavoursRepo baseTeasRepo)
		{
			_FlavoursRepo = baseTeasRepo ?? throw new ArgumentNullException(nameof(baseTeasRepo));
		}

		[Route("")]
		[HttpGet]
		public async Task<IActionResult> GetFlavours()
		{
			var flavours = await _FlavoursRepo.GetFlavours();

			return Ok(flavours);
		}
	}
}