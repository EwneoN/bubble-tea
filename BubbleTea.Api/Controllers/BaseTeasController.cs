﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BubbleTea.Core.Repos.BaseTeas;
using Microsoft.AspNetCore.Mvc;

namespace BubbleTea.Api.Controllers
{

	[Route("/base-teas")]
	public class BaseTeasController : ControllerBase
	{
		//usually i'd use a service which is injected with a repo rather than a repo directly
		//but for speed im just using the repo here
		private readonly IBaseTeasRepo _BaseTeasRepo;

		public BaseTeasController(IBaseTeasRepo baseTeasRepo)
		{
			_BaseTeasRepo = baseTeasRepo ?? throw new ArgumentNullException(nameof(baseTeasRepo));
		}

		[Route("")]
		[HttpGet]
		public async Task<IActionResult> GetBaseTeas()
		{
			var baseTeas = await _BaseTeasRepo.GetBaseTeas();

			return Ok(baseTeas);
		}
	}
}
