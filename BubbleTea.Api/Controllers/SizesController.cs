﻿using System;
using System.Threading.Tasks;
using BubbleTea.Core.Repos.Sizes;
using Microsoft.AspNetCore.Mvc;

namespace BubbleTea.Api.Controllers
{
	[Route("/sizes")]
	public class SizesController : ControllerBase
	{
		//usually i'd use a service which is injected with a repo rather than a repo directly
		//but for speed im just using the repo here
		private readonly ISizesRepo _SizesRepo;

		public SizesController(ISizesRepo baseTeasRepo)
		{
			_SizesRepo = baseTeasRepo ?? throw new ArgumentNullException(nameof(baseTeasRepo));
		}

		[Route("")]
		[HttpGet]
		public async Task<IActionResult> GetSizes()
		{
			var sizes = await _SizesRepo.GetSizes();

			return Ok(sizes);
		}
	}
}