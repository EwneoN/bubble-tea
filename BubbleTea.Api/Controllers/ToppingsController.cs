﻿using System;
using System.Threading.Tasks;
using BubbleTea.Core.Repos.Toppings;
using Microsoft.AspNetCore.Mvc;

namespace BubbleTea.Api.Controllers
{
	[Route("/toppings")]
	public class ToppingsController : ControllerBase
	{
		//usually i'd use a service which is injected with a repo rather than a repo directly
		//but for speed im just using the repo here
		private readonly IToppingsRepo _ToppingsRepo;

		public ToppingsController(IToppingsRepo baseTeasRepo)
		{
			_ToppingsRepo = baseTeasRepo ?? throw new ArgumentNullException(nameof(baseTeasRepo));
		}

		[Route("")]
		[HttpGet]
		public async Task<IActionResult> GetToppings()
		{
			var toppings = await _ToppingsRepo.GetToppings();

			return Ok(toppings);
		}
	}
}