﻿using System;

namespace BubbleTea.Core.Services.Time
{
	public class TimeService : ITimeService
	{
		public DateTimeOffset GetNow()
		{
			return DateTimeOffset.Now;
		}
	}
}