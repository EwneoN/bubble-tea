﻿using System;

namespace BubbleTea.Core.Services.Time
{
	public interface ITimeService
	{
		DateTimeOffset GetNow();
	}
}
