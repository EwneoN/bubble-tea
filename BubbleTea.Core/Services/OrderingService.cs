﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BubbleTea.Core.Dtos;
using BubbleTea.Core.Models;
using BubbleTea.Core.Repos.Orders;

namespace BubbleTea.Core.Services
{
	public class OrderingService : IOrderingService
	{
		private readonly IOrdersRepo _OrdersRepo;

		public OrderingService(IOrdersRepo ordersRepo)
		{
			_OrdersRepo = ordersRepo ?? throw new ArgumentNullException(nameof(ordersRepo));
		}

		public async Task<BubbleTeaOrder> GetOrder(Guid id)
		{
			Order order = await _OrdersRepo.GetOrder(id);

			return MakeBubbleTeaOrder(order);
		}

		public async Task<ICollection<BubbleTeaOrder>> GetOrders()
		{
			ICollection<Order> orders = await _OrdersRepo.GetOrders();

			return orders
				.Select(MakeBubbleTeaOrder)
				.ToList();
		}

		public async Task<BubbleTeaOrder> CreateOrder(NewBubbleTeaOrder newOrder)
		{
			Order order = await _OrdersRepo.CreateOrder(newOrder);

			return MakeBubbleTeaOrder(order);
		}

		public async Task<BubbleTeaOrder> UpdateOrder(BubbleTeaOrderUpdate orderUpdate)
		{
			Order order = await _OrdersRepo.UpdateOrder(orderUpdate);

			return MakeBubbleTeaOrder(order);
		}

		public async Task DeleteOrder(Guid id)
		{
			await _OrdersRepo.DeleteOrder(id);
		}

		private BubbleTeaOrder MakeBubbleTeaOrder(Order order)
		{
			var cost = order.Size.PriceModifier *
			           order.BaseTea.Price + order.OrderToppings.Sum(t => t.Topping.Price);

			return new BubbleTeaOrder
			{
				Id = order.Id,
				SizeId = order.SizeId,
				BaseTeaId = order.BaseTeaId,
				FlavourId = order.FlavourId,
				CustomerId = order.CustomerId,
				Size = order.Size.Name,
				BaseTea = order.BaseTea.Name,
				Flavour = order.Flavour?.Name,
				CustomerName = order.Customer.Name,
				Toppings = order.OrderToppings
					.Select(t => new BubbleTeaTopping { Id = t.Topping.Id, Name = t.Topping.Name })
					.ToArray(),
				Price = Math.Ceiling(cost * 100) / 100
			};
		}
	}
}