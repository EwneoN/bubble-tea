﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BubbleTea.Core.Services.Ids
{
	public interface IIdsService
	{
		Guid GetNewId();
	}
}
