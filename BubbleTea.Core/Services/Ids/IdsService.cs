﻿using System;

namespace BubbleTea.Core.Services.Ids
{
	public class IdsService : IIdsService
	{
		public Guid GetNewId()
		{
			return Guid.NewGuid();
		}
	}
}