﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BubbleTea.Core.Dtos;

namespace BubbleTea.Core.Services
{
	public interface IOrderingService
	{
		Task<BubbleTeaOrder> GetOrder(Guid id);
		Task<ICollection<BubbleTeaOrder>> GetOrders();
		Task<BubbleTeaOrder> CreateOrder(NewBubbleTeaOrder newOrder);
		Task<BubbleTeaOrder> UpdateOrder(BubbleTeaOrderUpdate orderUpdate);
		Task DeleteOrder(Guid id);
	}
}
