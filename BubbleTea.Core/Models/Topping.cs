﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BubbleTea.Core.Models
{
	public class Topping : BaseModel
	{
		public string Name { get; set; }
		public decimal Price { get; set; }

		public ICollection<OrderTopping> OrderToppings { get; set; } = new HashSet<OrderTopping>();
	}
}
