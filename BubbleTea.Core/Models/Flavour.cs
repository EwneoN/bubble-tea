﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BubbleTea.Core.Models
{
	public class Flavour : BaseModel
	{
		public string Name { get; set; }

		public ICollection<Order> Orders { get; set; } = new HashSet<Order>();
	}
}
