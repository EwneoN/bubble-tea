﻿using System;

namespace BubbleTea.Core.Models
{
	public abstract class BaseModel
	{
		public Guid Id { get; set; } 
		public DateTimeOffset Timestamp { get; set; } 
	}
}
