﻿using System;
using System.Collections.Generic;

namespace BubbleTea.Core.Models
{
	public class Order : BaseModel
	{
		public Guid SizeId { get; set; }
		public Guid BaseTeaId { get; set; }
		public Guid? FlavourId { get; set; }
		public Guid CustomerId { get; set; }

		public Size Size { get; set; }
		public BaseTea BaseTea { get; set; }
		public Flavour Flavour { get; set; }
		public Customer Customer { get; set; }

		public ICollection<OrderTopping> OrderToppings { get; set; } = new HashSet<OrderTopping>();
	}
}
