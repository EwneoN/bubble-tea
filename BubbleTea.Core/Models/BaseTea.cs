﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BubbleTea.Core.Models
{
	public class BaseTea : BaseModel
	{
		public string Name { get; set; }
		public decimal Price { get; set; }

		public ICollection<Order> Orders { get; set; } = new HashSet<Order>();
	}
}
