﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BubbleTea.Core.Models
{
	public class Customer : BaseModel
	{
		public string Name { get; set; }
		public DateTimeOffset DateOfBirth { get; set; }

		public ICollection<Order> Orders { get; set; } = new HashSet<Order>();
	}
}