﻿using System;

namespace BubbleTea.Core.Models
{
	public class OrderTopping : BaseModel
	{
		public Guid OrderId { get; set; }
		public Guid ToppingId { get; set; }

		public Order Order { get; set; }
		public Topping Topping { get; set; }
	}
}
