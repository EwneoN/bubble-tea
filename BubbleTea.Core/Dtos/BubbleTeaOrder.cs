﻿using System;

namespace BubbleTea.Core.Dtos
{
	public class BubbleTeaOrder
	{
		public Guid Id { get; set; }
		public Guid SizeId { get; set; }
		public Guid? FlavourId { get; set; }
		public Guid BaseTeaId { get; set; }
		public Guid CustomerId { get; set; }
		public string Size { get; set; }
		public string Flavour { get; set; }
		public string BaseTea { get; set; }
		public string CustomerName { get; set; }
		public BubbleTeaTopping[] Toppings { get; set; }
		public decimal Price { get; set; }
	}
}
