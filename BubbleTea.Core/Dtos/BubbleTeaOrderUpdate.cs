﻿using System;

namespace BubbleTea.Core.Dtos
{
	public class BubbleTeaOrderUpdate
	{
		public Guid Id { get; set; }
		public Guid SizeId { get; set; }
		public Guid FlavourId { get; set; }
		public Guid BaseTeaId { get; set; }
		public Guid CustomerId { get; set; }
		public Guid[] ToppingIds { get; set; }
	}
}