﻿using System;

namespace BubbleTea.Core.Dtos.Toppings
{
	public class ToppingUpdate
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public decimal Price { get; set; }
	}
}