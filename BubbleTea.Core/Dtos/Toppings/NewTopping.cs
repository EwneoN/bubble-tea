﻿namespace BubbleTea.Core.Dtos.Toppings
{
	public class NewTopping
	{
		public string Name { get; set; }
		public decimal Price { get; set; }
	}
}