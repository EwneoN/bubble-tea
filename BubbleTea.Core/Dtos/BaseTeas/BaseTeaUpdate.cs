﻿using System;

namespace BubbleTea.Core.Dtos.BaseTeas
{
	public class BaseTeaUpdate
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public decimal Price { get; set; }
	}
}