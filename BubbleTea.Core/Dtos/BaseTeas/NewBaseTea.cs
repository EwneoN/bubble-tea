﻿namespace BubbleTea.Core.Dtos.BaseTeas
{
	public class NewBaseTea
	{
		public string Name { get; set; }
		public decimal Price { get; set; }
	}
}
