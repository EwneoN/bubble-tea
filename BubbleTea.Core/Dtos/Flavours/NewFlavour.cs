﻿namespace BubbleTea.Core.Dtos.Flavours
{
	public class NewFlavour
	{
		public string Name { get; set; }
	}
}