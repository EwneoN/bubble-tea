﻿using System;

namespace BubbleTea.Core.Dtos.Flavours
{
	public class FlavourUpdate
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
	}
}