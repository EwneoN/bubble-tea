﻿using System;
using System.Text.Json.Serialization;

namespace BubbleTea.Core.Dtos
{
	public class NewBubbleTeaOrder
	{
		[JsonPropertyName("sizeId")]
		public Guid SizeId { get; set; }
		[JsonPropertyName("flavourId")]
		public Guid FlavourId { get; set; }
		[JsonPropertyName("baseTeaId")]
		public Guid BaseTeaId { get; set; }
		[JsonPropertyName("customerId")]
		public Guid CustomerId { get; set; }
		[JsonPropertyName("toppingIds")]
		public Guid[] ToppingIds { get; set; }
	}
}