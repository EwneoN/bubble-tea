﻿using System;

namespace BubbleTea.Core.Dtos.Customers
{
	public class CustomerUpdate
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public DateTimeOffset DateOfBirth { get; set; }
	}
}