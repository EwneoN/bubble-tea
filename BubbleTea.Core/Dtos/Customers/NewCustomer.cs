﻿using System;

namespace BubbleTea.Core.Dtos.Customers
{
	public class NewCustomer
	{
		public string Name { get; set; }
		public DateTimeOffset DateOfBirth { get; set; }
	}
}
