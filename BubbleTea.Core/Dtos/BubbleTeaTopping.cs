﻿using System;

namespace BubbleTea.Core.Dtos
{
	public class BubbleTeaTopping
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
	}
}