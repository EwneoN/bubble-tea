﻿using System;
using BubbleTea.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BubbleTea.Core.Persistence.EntityConfigs
{
	public class CustomerConfig : IEntityTypeConfiguration<Customer>
	{
		public void Configure(EntityTypeBuilder<Customer> builder)
		{
			builder.HasKey(e => e.Id);
			builder.Property(e => e.Id)
				.ValueGeneratedNever();

			builder.Property(e => e.Name)
				.HasMaxLength(256)
				.IsRequired();

			builder.Property(e => e.DateOfBirth)
				.IsRequired();

			builder.Property(e => e.Timestamp)
				.IsRequired();

			builder.HasIndex(e => e.Name)
				.IsUnique();

			builder.HasMany(e => e.Orders)
				.WithOne(e => e.Customer)
				.HasForeignKey(e => e.CustomerId);

			// normally in a real life project we would use a seeder so we can use the id and time services
			// but for simplicity sakes we are just initialising the data here
			builder.HasData(
				new Customer { Id = Guid.NewGuid(), Name = "Scott", Timestamp = DateTimeOffset.Now, 
					DateOfBirth = new DateTimeOffset(1980, 1, 1, 0, 0, 0, TimeSpan.FromHours(10))
				},
				new Customer { Id = Guid.NewGuid(), Name = "Todd", Timestamp = DateTimeOffset.Now,
					DateOfBirth = new DateTimeOffset(1985, 1, 1, 0, 0, 0, TimeSpan.FromHours(10))
				},
				new Customer { Id = Guid.NewGuid(), Name = "Marsha", Timestamp = DateTimeOffset.Now,
					DateOfBirth = new DateTimeOffset(1990, 1, 1, 0, 0, 0, TimeSpan.FromHours(10))
				},
				new Customer { Id = Guid.NewGuid(), Name = "Bill", Timestamp = DateTimeOffset.Now,
					DateOfBirth = new DateTimeOffset(1995, 1, 1, 0, 0, 0, TimeSpan.FromHours(10))
				},
				new Customer { Id = Guid.NewGuid(), Name = "Jolene", Timestamp = DateTimeOffset.Now,
					DateOfBirth = new DateTimeOffset(2000, 1, 1, 0, 0, 0, TimeSpan.FromHours(10))
				});
		}
	}
}