﻿using System;
using BubbleTea.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BubbleTea.Core.Persistence.EntityConfigs
{
	public class SizeConfig : IEntityTypeConfiguration<Size>
	{
		public void Configure(EntityTypeBuilder<Size> builder)
		{
			builder.HasKey(e => e.Id);
			builder.Property(e => e.Id)
				.ValueGeneratedNever();

			builder.Property(e => e.Name)
				.HasMaxLength(256)
				.IsRequired();

			builder.Property(e => e.PriceModifier)
				.IsRequired();

			builder.Property(e => e.Timestamp)
				.IsRequired();

			builder.HasIndex(e => e.Name)
				.IsUnique();

			builder.HasMany(e => e.Orders)
				.WithOne(e => e.Size)
				.HasForeignKey(e => e.SizeId);

			// normally in a real life project we would use a seeder so we can use the id and time services
			// but for simplicity sakes we are just initialising the data here
			builder.HasData(
				new Size {Id = Guid.NewGuid(), Name = "Small", PriceModifier = 1, Timestamp = DateTimeOffset.Now},
				new Size {Id = Guid.NewGuid(), Name = "Medium", PriceModifier = 1.25M, Timestamp = DateTimeOffset.Now},
				new Size {Id = Guid.NewGuid(), Name = "Large", PriceModifier = 1.5M, Timestamp = DateTimeOffset.Now});
		}
	}
}