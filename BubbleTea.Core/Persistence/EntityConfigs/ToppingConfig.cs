﻿using System;
using BubbleTea.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BubbleTea.Core.Persistence.EntityConfigs
{
	public class ToppingConfig : IEntityTypeConfiguration<Topping>
	{
		public void Configure(EntityTypeBuilder<Topping> builder)
		{
			builder.HasKey(e => e.Id);
			builder.Property(e => e.Id)
				.ValueGeneratedNever();

			builder.Property(e => e.Name)
				.HasMaxLength(256)
				.IsRequired();

			builder.Property(e => e.Price)
				.IsRequired();

			builder.Property(e => e.Timestamp)
				.IsRequired();

			builder.HasIndex(e => e.Name)
				.IsUnique();

			builder.HasMany(e => e.OrderToppings)
				.WithOne(e => e.Topping)
				.HasForeignKey(e => e.ToppingId);

			// normally in a real life project we would use a seeder so we can use the id and time services
			// but for simplicity sakes we are just initialising the data here
			builder.HasData(
				new Topping { Id = Guid.NewGuid(), Name = "Boba", Price = 1, Timestamp = DateTimeOffset.Now },
				new Topping { Id = Guid.NewGuid(), Name = "Red Bean", Price = 1, Timestamp = DateTimeOffset.Now },
				new Topping { Id = Guid.NewGuid(), Name = "Ai-Yu Jelly", Price = 1.5M, Timestamp = DateTimeOffset.Now },
				new Topping { Id = Guid.NewGuid(), Name = "Basil Seeds", Price = 1.5M, Timestamp = DateTimeOffset.Now });
		}
	}
}