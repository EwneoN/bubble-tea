﻿using BubbleTea.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BubbleTea.Core.Persistence.EntityConfigs
{
	public class OrderConfig : IEntityTypeConfiguration<Order>
	{
		public void Configure(EntityTypeBuilder<Order> builder)
		{
			builder.HasKey(e => e.Id);
			builder.Property(e => e.Id)
				.ValueGeneratedNever();

			builder.Property(e => e.SizeId)
				.IsRequired();

			builder.Property(e => e.BaseTeaId)
				.IsRequired();

			builder.Property(e => e.FlavourId)
				.IsRequired(false);

			builder.Property(e => e.CustomerId)
				.IsRequired();

			builder.Property(e => e.Timestamp)
				.IsRequired();

			builder.HasOne(e => e.Size)
				.WithMany(e => e.Orders)
				.HasForeignKey(e => e.SizeId);

			builder.HasOne(e => e.BaseTea)
				.WithMany(e => e.Orders)
				.HasForeignKey(e => e.BaseTeaId);

			builder.HasOne(e => e.Flavour)
				.WithMany(e => e.Orders)
				.HasForeignKey(e => e.FlavourId)
				.IsRequired(false);

			builder.HasOne(e => e.Customer)
				.WithMany(e => e.Orders)
				.HasForeignKey(e => e.CustomerId);

			builder.HasMany(e => e.OrderToppings)
				.WithOne(e => e.Order)
				.HasForeignKey(e => e.OrderId);
		}
	}
}
