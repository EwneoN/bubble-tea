﻿using BubbleTea.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BubbleTea.Core.Persistence.EntityConfigs
{
	public class OrderToppingConfig : IEntityTypeConfiguration<OrderTopping>
	{
		public void Configure(EntityTypeBuilder<OrderTopping> builder)
		{
			builder.HasKey(e => e.Id);
			builder.Property(e => e.Id)
				.ValueGeneratedNever();

			builder.Property(e => e.OrderId)
				.IsRequired();

			builder.Property(e => e.ToppingId)
				.IsRequired();

			builder.Property(e => e.Timestamp)
				.IsRequired();

			builder.HasOne(e => e.Order)
				.WithMany(e => e.OrderToppings)
				.HasForeignKey(e => e.OrderId);

			builder.HasOne(e => e.Topping)
				.WithMany(e => e.OrderToppings)
				.HasForeignKey(e => e.ToppingId);
		}
	}
}