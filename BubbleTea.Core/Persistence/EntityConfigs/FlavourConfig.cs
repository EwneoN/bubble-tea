﻿using System;
using BubbleTea.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BubbleTea.Core.Persistence.EntityConfigs
{
	public class FlavourConfig : IEntityTypeConfiguration<Flavour>
	{
		public void Configure(EntityTypeBuilder<Flavour> builder)
		{
			builder.HasKey(e => e.Id);
			builder.Property(e => e.Id)
				.ValueGeneratedNever();

			builder.Property(e => e.Name)
				.HasMaxLength(256)
				.IsRequired();

			builder.Property(e => e.Timestamp)
				.IsRequired();

			builder.HasIndex(e => e.Name)
				.IsUnique();

			builder.HasMany(e => e.Orders)
				.WithOne(e => e.Flavour)
				.HasForeignKey(e => e.FlavourId)
				.IsRequired(false);

			// normally in a real life project we would use a seeder so we can use the id and time services
			// but for simplicity sakes we are just initialising the data here
			builder.HasData(
				new Flavour { Id = Guid.NewGuid(), Name = "Lemon", Timestamp = DateTimeOffset.Now },
				new Flavour { Id = Guid.NewGuid(), Name = "PasisonFruit", Timestamp = DateTimeOffset.Now },
				new Flavour { Id = Guid.NewGuid(), Name = "Milk Tea", Timestamp = DateTimeOffset.Now });
		}
	}
}