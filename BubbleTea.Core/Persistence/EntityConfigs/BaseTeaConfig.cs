﻿using System;
using BubbleTea.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BubbleTea.Core.Persistence.EntityConfigs
{
	public class BaseTeaConfig : IEntityTypeConfiguration<BaseTea>
	{
		public void Configure(EntityTypeBuilder<BaseTea> builder)
		{
			builder.HasKey(e => e.Id);
			builder.Property(e => e.Id)
				.ValueGeneratedNever();

			builder.Property(e => e.Name)
				.HasMaxLength(256)
				.IsRequired();

			builder.Property(e => e.Price)
				.IsRequired();

			builder.Property(e => e.Timestamp)
				.IsRequired();

			builder.HasIndex(e => e.Name)
				.IsUnique();

			builder.HasMany(e => e.Orders)
				.WithOne(e => e.BaseTea)
				.HasForeignKey(e => e.BaseTeaId);

			// normally in a real life project we would use a seeder so we can use the id and time services
			// but for simplicity sakes we are just initialising the data here
			builder.HasData(
				new BaseTea { Id = Guid.NewGuid(), Name = "Green Tea", Price = 1, Timestamp = DateTimeOffset.Now },
				new BaseTea { Id = Guid.NewGuid(), Name = "Black Tea", Price = 1, Timestamp = DateTimeOffset.Now },
				new BaseTea { Id = Guid.NewGuid(), Name = "Milk Tea", Price = 1.5M, Timestamp = DateTimeOffset.Now });
		}
	}
}
