﻿namespace BubbleTea.Core.Persistence
{
	public class BubbleTeaDbContextConfig
	{
		public string ConnectionString { get; set; }
	}
}