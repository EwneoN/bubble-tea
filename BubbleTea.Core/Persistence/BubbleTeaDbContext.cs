﻿using System;
using System.Threading;
using System.Threading.Tasks;
using BubbleTea.Core.Models;
using BubbleTea.Core.Services.Ids;
using BubbleTea.Core.Services.Time;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace BubbleTea.Core.Persistence
{
	public class BubbleTeaDbContext : DbContext
	{
		private readonly ITimeService _TimeService;
		private readonly IIdsService _IdsService;

		public DbSet<BaseTea> BaseTeas { get; set; }
		public DbSet<Customer> Customers { get; set; }
		public DbSet<Flavour> Flavours { get; set; }
		public DbSet<Order> Orders { get; set; }
		public DbSet<OrderTopping> OrderToppings { get; set; }
		public DbSet<Size> Sizes { get; set; }
		public DbSet<Topping> Toppings { get; set; }

		public BubbleTeaDbContext(DbContextOptions<BubbleTeaDbContext> options, ITimeService timeService, IIdsService idsService)
		: base(options)
		{
			_TimeService = timeService ?? throw new ArgumentNullException(nameof(timeService));
			_IdsService = idsService ?? throw new ArgumentNullException(nameof(idsService));
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.ApplyConfigurationsFromAssembly(typeof(BubbleTeaDbContext).Assembly);
		}

		// we are only overriding this method for this project. normally we would override all other save methods too
		public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
		{
			foreach (EntityEntry<BaseModel> entry in ChangeTracker.Entries<BaseModel>())
			{
				if (entry.State == EntityState.Added && entry.Entity.Id == Guid.Empty)
				{
					entry.Entity.Id = _IdsService.GetNewId();
				}

				entry.Entity.Timestamp = _TimeService.GetNow();
			}

			return await base.SaveChangesAsync(cancellationToken);
		}
	}
}
