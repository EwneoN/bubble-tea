﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BubbleTea.Core.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BaseTeas",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Timestamp = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseTeas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    Timestamp = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Flavours",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    Timestamp = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Flavours", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sizes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    PriceModifier = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Timestamp = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sizes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Toppings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Timestamp = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Toppings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SizeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    BaseTeaId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FlavourId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CustomerId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Timestamp = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_BaseTeas_BaseTeaId",
                        column: x => x.BaseTeaId,
                        principalTable: "BaseTeas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_Flavours_FlavourId",
                        column: x => x.FlavourId,
                        principalTable: "Flavours",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_Sizes_SizeId",
                        column: x => x.SizeId,
                        principalTable: "Sizes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderToppings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OrderId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ToppingId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Timestamp = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderToppings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderToppings_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderToppings_Toppings_ToppingId",
                        column: x => x.ToppingId,
                        principalTable: "Toppings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "BaseTeas",
                columns: new[] { "Id", "Name", "Price", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("33c16b34-e218-4ba2-9e9f-32aa7d7f2734"), "Green Tea", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 680, DateTimeKind.Unspecified).AddTicks(9398), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("c448546d-d80b-4172-8ff2-4934ab4faff1"), "Black Tea", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 682, DateTimeKind.Unspecified).AddTicks(9060), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("9b81786e-4f68-4968-8690-3c2b845a45c2"), "Milk Tea", 1.5m, new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 682, DateTimeKind.Unspecified).AddTicks(9110), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Name", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("11eb8e8d-ad8c-4a17-afca-15c03a7e62cb"), "Scott", new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 686, DateTimeKind.Unspecified).AddTicks(3579), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("9433fc17-db68-46e1-936a-385e94a64c5f"), "Todd", new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 686, DateTimeKind.Unspecified).AddTicks(3618), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("e54f85c1-b5e5-4608-9a04-9628dad7ccef"), "Marsha", new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 686, DateTimeKind.Unspecified).AddTicks(3622), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("5f752355-8c4b-4936-a6f4-7e115961ad5f"), "Bill", new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 686, DateTimeKind.Unspecified).AddTicks(3625), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("a9ba7a06-8d23-44ff-8600-7639e1341fba"), "Jolene", new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 686, DateTimeKind.Unspecified).AddTicks(3628), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Flavours",
                columns: new[] { "Id", "Name", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("b6ea2c3e-83f3-4704-9b90-37f5fd6cc094"), "Milk Tea", new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 688, DateTimeKind.Unspecified).AddTicks(1813), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("73b53af0-7d7e-462a-818e-6ff738193e04"), "Lemon", new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 688, DateTimeKind.Unspecified).AddTicks(1766), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("2ac46f2b-4dda-4aa0-b3a0-448c9e52dc3e"), "PasisonFruit", new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 688, DateTimeKind.Unspecified).AddTicks(1809), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Sizes",
                columns: new[] { "Id", "Name", "PriceModifier", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("42f47479-f9da-40fe-87b8-795a425492d0"), "Small", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 695, DateTimeKind.Unspecified).AddTicks(5313), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("9e7534c1-fb6b-4b4b-843a-5cd254a37588"), "Medium", 1.25m, new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 695, DateTimeKind.Unspecified).AddTicks(5371), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("d68530d8-a6ac-4b09-af41-23226d7b5d3a"), "Large", 1.5m, new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 695, DateTimeKind.Unspecified).AddTicks(5376), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Toppings",
                columns: new[] { "Id", "Name", "Price", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("05e821dd-5466-4d00-b3d6-31869a4566cd"), "Black Tea", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 697, DateTimeKind.Unspecified).AddTicks(1644), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("b2a3ed59-df77-4163-bba6-607f9f8edff1"), "Green Tea", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 697, DateTimeKind.Unspecified).AddTicks(1599), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("64b2b0f6-72bf-4f84-8784-47baa620e09f"), "Milk Tea", 1.5m, new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 697, DateTimeKind.Unspecified).AddTicks(1655), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.CreateIndex(
                name: "IX_BaseTeas_Name",
                table: "BaseTeas",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Customers_Name",
                table: "Customers",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Flavours_Name",
                table: "Flavours",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Orders_BaseTeaId",
                table: "Orders",
                column: "BaseTeaId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CustomerId",
                table: "Orders",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_FlavourId",
                table: "Orders",
                column: "FlavourId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_SizeId",
                table: "Orders",
                column: "SizeId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderToppings_OrderId",
                table: "OrderToppings",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderToppings_ToppingId",
                table: "OrderToppings",
                column: "ToppingId");

            migrationBuilder.CreateIndex(
                name: "IX_Sizes_Name",
                table: "Sizes",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Toppings_Name",
                table: "Toppings",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrderToppings");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Toppings");

            migrationBuilder.DropTable(
                name: "BaseTeas");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "Flavours");

            migrationBuilder.DropTable(
                name: "Sizes");
        }
    }
}
