﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BubbleTea.Core.Migrations
{
    public partial class NewToppings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "BaseTeas",
                keyColumn: "Id",
                keyValue: new Guid("af8bde64-342c-4ada-aab4-67439b21ceb0"));

            migrationBuilder.DeleteData(
                table: "BaseTeas",
                keyColumn: "Id",
                keyValue: new Guid("e926435a-6520-4193-99ed-1f3b7b5bf369"));

            migrationBuilder.DeleteData(
                table: "BaseTeas",
                keyColumn: "Id",
                keyValue: new Guid("fdbfe3c7-4f22-49ff-ab3a-acb079848a5e"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("14d09ba5-aa7a-480c-9160-9b1991ef2cb7"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("a02e590e-936d-4a9e-9ba8-22dcff3c1773"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("a164c1aa-f822-4217-b9ad-fae3ec211812"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("a2420ceb-60ca-4277-9ea1-935cbd2343c9"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("fc7d3d15-8496-4bad-a324-ba58da7ab1b0"));

            migrationBuilder.DeleteData(
                table: "Flavours",
                keyColumn: "Id",
                keyValue: new Guid("79baab7f-19e7-499d-b05d-cda2f1f24360"));

            migrationBuilder.DeleteData(
                table: "Flavours",
                keyColumn: "Id",
                keyValue: new Guid("944ee5e2-12d6-4289-89bd-da9a096c57fc"));

            migrationBuilder.DeleteData(
                table: "Flavours",
                keyColumn: "Id",
                keyValue: new Guid("fa2a980d-9d50-49ff-bd0a-97be683353fd"));

            migrationBuilder.DeleteData(
                table: "Sizes",
                keyColumn: "Id",
                keyValue: new Guid("008339ab-bf9a-4478-8bd5-516dd64e1d13"));

            migrationBuilder.DeleteData(
                table: "Sizes",
                keyColumn: "Id",
                keyValue: new Guid("4ab51dab-d52e-4c89-a3bd-0ae33065b5d9"));

            migrationBuilder.DeleteData(
                table: "Sizes",
                keyColumn: "Id",
                keyValue: new Guid("84b61fc0-7f77-41a2-ba31-31b33c3136f4"));

            migrationBuilder.DeleteData(
                table: "Toppings",
                keyColumn: "Id",
                keyValue: new Guid("209c9e40-7a73-4ef8-ba02-5a6b7ce4204a"));

            migrationBuilder.DeleteData(
                table: "Toppings",
                keyColumn: "Id",
                keyValue: new Guid("cdbcbbdd-7b92-4f90-af85-3de8bf335cb6"));

            migrationBuilder.DeleteData(
                table: "Toppings",
                keyColumn: "Id",
                keyValue: new Guid("ddf4ca07-d281-4a82-8bee-d2785c213a30"));

            migrationBuilder.InsertData(
                table: "BaseTeas",
                columns: new[] { "Id", "Name", "Price", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("44d5dc17-7fd3-4919-913d-fed06e1a89e0"), "Green Tea", 1m, new DateTimeOffset(new DateTime(2021, 1, 29, 10, 53, 18, 763, DateTimeKind.Unspecified).AddTicks(1741), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("e3793f2d-84a4-4352-ac7f-f80712580edd"), "Black Tea", 1m, new DateTimeOffset(new DateTime(2021, 1, 29, 10, 53, 18, 765, DateTimeKind.Unspecified).AddTicks(1042), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("4043ec44-59b1-4c30-8cbb-6c62fafa3c85"), "Milk Tea", 1.5m, new DateTimeOffset(new DateTime(2021, 1, 29, 10, 53, 18, 765, DateTimeKind.Unspecified).AddTicks(1088), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "DateOfBirth", "Name", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("9b534b67-33aa-41fb-8768-5a5dac3e9196"), new DateTimeOffset(new DateTime(1980, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 10, 0, 0, 0)), "Scott", new DateTimeOffset(new DateTime(2021, 1, 29, 10, 53, 18, 769, DateTimeKind.Unspecified).AddTicks(697), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("7b38cf50-847d-4887-8d9e-ebb06cf07d93"), new DateTimeOffset(new DateTime(1985, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 10, 0, 0, 0)), "Todd", new DateTimeOffset(new DateTime(2021, 1, 29, 10, 53, 18, 769, DateTimeKind.Unspecified).AddTicks(1328), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("5a57f9cd-03ff-44ba-a5f4-a5661f3301e0"), new DateTimeOffset(new DateTime(1990, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 10, 0, 0, 0)), "Marsha", new DateTimeOffset(new DateTime(2021, 1, 29, 10, 53, 18, 769, DateTimeKind.Unspecified).AddTicks(1351), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("a94dd46d-9ede-4945-96e6-980b5fc706d9"), new DateTimeOffset(new DateTime(1995, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 10, 0, 0, 0)), "Bill", new DateTimeOffset(new DateTime(2021, 1, 29, 10, 53, 18, 769, DateTimeKind.Unspecified).AddTicks(1355), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("81ae349f-963d-483a-9c16-ea69029d83b1"), new DateTimeOffset(new DateTime(2000, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 10, 0, 0, 0)), "Jolene", new DateTimeOffset(new DateTime(2021, 1, 29, 10, 53, 18, 769, DateTimeKind.Unspecified).AddTicks(1358), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Flavours",
                columns: new[] { "Id", "Name", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("af8425d4-8160-455f-a2b6-c6fd7351808c"), "Milk Tea", new DateTimeOffset(new DateTime(2021, 1, 29, 10, 53, 18, 770, DateTimeKind.Unspecified).AddTicks(8529), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("4b57f632-2ce0-4369-ab09-534b1b2f95c8"), "Lemon", new DateTimeOffset(new DateTime(2021, 1, 29, 10, 53, 18, 770, DateTimeKind.Unspecified).AddTicks(8487), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("bd08372a-0dad-4ff9-89b9-85e682f78639"), "PasisonFruit", new DateTimeOffset(new DateTime(2021, 1, 29, 10, 53, 18, 770, DateTimeKind.Unspecified).AddTicks(8525), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Sizes",
                columns: new[] { "Id", "Name", "PriceModifier", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("22db01db-a7e6-4518-b4f3-dab3f94f7449"), "Small", 1m, new DateTimeOffset(new DateTime(2021, 1, 29, 10, 53, 18, 777, DateTimeKind.Unspecified).AddTicks(9600), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("8df8c6ba-c93f-4303-ade0-5f18597eef6a"), "Medium", 1.25m, new DateTimeOffset(new DateTime(2021, 1, 29, 10, 53, 18, 777, DateTimeKind.Unspecified).AddTicks(9679), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("2cadf0d1-8b38-4fd0-b583-88af06729aea"), "Large", 1.5m, new DateTimeOffset(new DateTime(2021, 1, 29, 10, 53, 18, 777, DateTimeKind.Unspecified).AddTicks(9684), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Toppings",
                columns: new[] { "Id", "Name", "Price", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("02057a47-ede0-4ab6-be2a-42f16e193701"), "Ai-Yu Jelly", 1.5m, new DateTimeOffset(new DateTime(2021, 1, 29, 10, 53, 18, 779, DateTimeKind.Unspecified).AddTicks(6957), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("81011c53-a288-4da8-9b94-2006bb1b631c"), "Boba", 1m, new DateTimeOffset(new DateTime(2021, 1, 29, 10, 53, 18, 779, DateTimeKind.Unspecified).AddTicks(6886), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("b9454b06-5eb5-47b4-8ba2-2f7e64c6fc18"), "Red Bean", 1m, new DateTimeOffset(new DateTime(2021, 1, 29, 10, 53, 18, 779, DateTimeKind.Unspecified).AddTicks(6943), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("556e4be3-32bb-4472-b92e-43f2eea0f4b0"), "Basil Seeds", 1.5m, new DateTimeOffset(new DateTime(2021, 1, 29, 10, 53, 18, 779, DateTimeKind.Unspecified).AddTicks(6961), new TimeSpan(0, 10, 0, 0, 0)) }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "BaseTeas",
                keyColumn: "Id",
                keyValue: new Guid("4043ec44-59b1-4c30-8cbb-6c62fafa3c85"));

            migrationBuilder.DeleteData(
                table: "BaseTeas",
                keyColumn: "Id",
                keyValue: new Guid("44d5dc17-7fd3-4919-913d-fed06e1a89e0"));

            migrationBuilder.DeleteData(
                table: "BaseTeas",
                keyColumn: "Id",
                keyValue: new Guid("e3793f2d-84a4-4352-ac7f-f80712580edd"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("5a57f9cd-03ff-44ba-a5f4-a5661f3301e0"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("7b38cf50-847d-4887-8d9e-ebb06cf07d93"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("81ae349f-963d-483a-9c16-ea69029d83b1"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("9b534b67-33aa-41fb-8768-5a5dac3e9196"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("a94dd46d-9ede-4945-96e6-980b5fc706d9"));

            migrationBuilder.DeleteData(
                table: "Flavours",
                keyColumn: "Id",
                keyValue: new Guid("4b57f632-2ce0-4369-ab09-534b1b2f95c8"));

            migrationBuilder.DeleteData(
                table: "Flavours",
                keyColumn: "Id",
                keyValue: new Guid("af8425d4-8160-455f-a2b6-c6fd7351808c"));

            migrationBuilder.DeleteData(
                table: "Flavours",
                keyColumn: "Id",
                keyValue: new Guid("bd08372a-0dad-4ff9-89b9-85e682f78639"));

            migrationBuilder.DeleteData(
                table: "Sizes",
                keyColumn: "Id",
                keyValue: new Guid("22db01db-a7e6-4518-b4f3-dab3f94f7449"));

            migrationBuilder.DeleteData(
                table: "Sizes",
                keyColumn: "Id",
                keyValue: new Guid("2cadf0d1-8b38-4fd0-b583-88af06729aea"));

            migrationBuilder.DeleteData(
                table: "Sizes",
                keyColumn: "Id",
                keyValue: new Guid("8df8c6ba-c93f-4303-ade0-5f18597eef6a"));

            migrationBuilder.DeleteData(
                table: "Toppings",
                keyColumn: "Id",
                keyValue: new Guid("02057a47-ede0-4ab6-be2a-42f16e193701"));

            migrationBuilder.DeleteData(
                table: "Toppings",
                keyColumn: "Id",
                keyValue: new Guid("556e4be3-32bb-4472-b92e-43f2eea0f4b0"));

            migrationBuilder.DeleteData(
                table: "Toppings",
                keyColumn: "Id",
                keyValue: new Guid("81011c53-a288-4da8-9b94-2006bb1b631c"));

            migrationBuilder.DeleteData(
                table: "Toppings",
                keyColumn: "Id",
                keyValue: new Guid("b9454b06-5eb5-47b4-8ba2-2f7e64c6fc18"));

            migrationBuilder.InsertData(
                table: "BaseTeas",
                columns: new[] { "Id", "Name", "Price", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("af8bde64-342c-4ada-aab4-67439b21ceb0"), "Green Tea", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 808, DateTimeKind.Unspecified).AddTicks(8570), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("e926435a-6520-4193-99ed-1f3b7b5bf369"), "Black Tea", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 810, DateTimeKind.Unspecified).AddTicks(7609), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("fdbfe3c7-4f22-49ff-ab3a-acb079848a5e"), "Milk Tea", 1.5m, new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 810, DateTimeKind.Unspecified).AddTicks(7655), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "DateOfBirth", "Name", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("a2420ceb-60ca-4277-9ea1-935cbd2343c9"), new DateTimeOffset(new DateTime(1980, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 10, 0, 0, 0)), "Scott", new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 814, DateTimeKind.Unspecified).AddTicks(3630), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("14d09ba5-aa7a-480c-9160-9b1991ef2cb7"), new DateTimeOffset(new DateTime(1985, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 10, 0, 0, 0)), "Todd", new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 814, DateTimeKind.Unspecified).AddTicks(4233), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("a02e590e-936d-4a9e-9ba8-22dcff3c1773"), new DateTimeOffset(new DateTime(1990, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 10, 0, 0, 0)), "Marsha", new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 814, DateTimeKind.Unspecified).AddTicks(4257), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("fc7d3d15-8496-4bad-a324-ba58da7ab1b0"), new DateTimeOffset(new DateTime(1995, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 10, 0, 0, 0)), "Bill", new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 814, DateTimeKind.Unspecified).AddTicks(4261), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("a164c1aa-f822-4217-b9ad-fae3ec211812"), new DateTimeOffset(new DateTime(2000, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 10, 0, 0, 0)), "Jolene", new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 814, DateTimeKind.Unspecified).AddTicks(4264), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Flavours",
                columns: new[] { "Id", "Name", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("79baab7f-19e7-499d-b05d-cda2f1f24360"), "Milk Tea", new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 816, DateTimeKind.Unspecified).AddTicks(2001), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("944ee5e2-12d6-4289-89bd-da9a096c57fc"), "Lemon", new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 816, DateTimeKind.Unspecified).AddTicks(1953), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("fa2a980d-9d50-49ff-bd0a-97be683353fd"), "PasisonFruit", new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 816, DateTimeKind.Unspecified).AddTicks(1998), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Sizes",
                columns: new[] { "Id", "Name", "PriceModifier", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("84b61fc0-7f77-41a2-ba31-31b33c3136f4"), "Small", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 823, DateTimeKind.Unspecified).AddTicks(1216), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("008339ab-bf9a-4478-8bd5-516dd64e1d13"), "Medium", 1.25m, new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 823, DateTimeKind.Unspecified).AddTicks(1271), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("4ab51dab-d52e-4c89-a3bd-0ae33065b5d9"), "Large", 1.5m, new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 823, DateTimeKind.Unspecified).AddTicks(1275), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Toppings",
                columns: new[] { "Id", "Name", "Price", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("ddf4ca07-d281-4a82-8bee-d2785c213a30"), "Black Tea", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 824, DateTimeKind.Unspecified).AddTicks(7564), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("cdbcbbdd-7b92-4f90-af85-3de8bf335cb6"), "Green Tea", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 824, DateTimeKind.Unspecified).AddTicks(7513), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("209c9e40-7a73-4ef8-ba02-5a6b7ce4204a"), "Milk Tea", 1.5m, new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 824, DateTimeKind.Unspecified).AddTicks(7568), new TimeSpan(0, 10, 0, 0, 0)) }
                });
        }
    }
}
