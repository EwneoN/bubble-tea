﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BubbleTea.Core.Migrations
{
    public partial class AddedCustomerDoB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "BaseTeas",
                keyColumn: "Id",
                keyValue: new Guid("33c16b34-e218-4ba2-9e9f-32aa7d7f2734"));

            migrationBuilder.DeleteData(
                table: "BaseTeas",
                keyColumn: "Id",
                keyValue: new Guid("9b81786e-4f68-4968-8690-3c2b845a45c2"));

            migrationBuilder.DeleteData(
                table: "BaseTeas",
                keyColumn: "Id",
                keyValue: new Guid("c448546d-d80b-4172-8ff2-4934ab4faff1"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("11eb8e8d-ad8c-4a17-afca-15c03a7e62cb"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("5f752355-8c4b-4936-a6f4-7e115961ad5f"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("9433fc17-db68-46e1-936a-385e94a64c5f"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("a9ba7a06-8d23-44ff-8600-7639e1341fba"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("e54f85c1-b5e5-4608-9a04-9628dad7ccef"));

            migrationBuilder.DeleteData(
                table: "Flavours",
                keyColumn: "Id",
                keyValue: new Guid("2ac46f2b-4dda-4aa0-b3a0-448c9e52dc3e"));

            migrationBuilder.DeleteData(
                table: "Flavours",
                keyColumn: "Id",
                keyValue: new Guid("73b53af0-7d7e-462a-818e-6ff738193e04"));

            migrationBuilder.DeleteData(
                table: "Flavours",
                keyColumn: "Id",
                keyValue: new Guid("b6ea2c3e-83f3-4704-9b90-37f5fd6cc094"));

            migrationBuilder.DeleteData(
                table: "Sizes",
                keyColumn: "Id",
                keyValue: new Guid("42f47479-f9da-40fe-87b8-795a425492d0"));

            migrationBuilder.DeleteData(
                table: "Sizes",
                keyColumn: "Id",
                keyValue: new Guid("9e7534c1-fb6b-4b4b-843a-5cd254a37588"));

            migrationBuilder.DeleteData(
                table: "Sizes",
                keyColumn: "Id",
                keyValue: new Guid("d68530d8-a6ac-4b09-af41-23226d7b5d3a"));

            migrationBuilder.DeleteData(
                table: "Toppings",
                keyColumn: "Id",
                keyValue: new Guid("05e821dd-5466-4d00-b3d6-31869a4566cd"));

            migrationBuilder.DeleteData(
                table: "Toppings",
                keyColumn: "Id",
                keyValue: new Guid("64b2b0f6-72bf-4f84-8784-47baa620e09f"));

            migrationBuilder.DeleteData(
                table: "Toppings",
                keyColumn: "Id",
                keyValue: new Guid("b2a3ed59-df77-4163-bba6-607f9f8edff1"));

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "DateOfBirth",
                table: "Customers",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.InsertData(
                table: "BaseTeas",
                columns: new[] { "Id", "Name", "Price", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("af8bde64-342c-4ada-aab4-67439b21ceb0"), "Green Tea", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 808, DateTimeKind.Unspecified).AddTicks(8570), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("e926435a-6520-4193-99ed-1f3b7b5bf369"), "Black Tea", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 810, DateTimeKind.Unspecified).AddTicks(7609), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("fdbfe3c7-4f22-49ff-ab3a-acb079848a5e"), "Milk Tea", 1.5m, new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 810, DateTimeKind.Unspecified).AddTicks(7655), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "DateOfBirth", "Name", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("a2420ceb-60ca-4277-9ea1-935cbd2343c9"), new DateTimeOffset(new DateTime(1980, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 10, 0, 0, 0)), "Scott", new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 814, DateTimeKind.Unspecified).AddTicks(3630), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("14d09ba5-aa7a-480c-9160-9b1991ef2cb7"), new DateTimeOffset(new DateTime(1985, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 10, 0, 0, 0)), "Todd", new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 814, DateTimeKind.Unspecified).AddTicks(4233), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("a02e590e-936d-4a9e-9ba8-22dcff3c1773"), new DateTimeOffset(new DateTime(1990, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 10, 0, 0, 0)), "Marsha", new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 814, DateTimeKind.Unspecified).AddTicks(4257), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("fc7d3d15-8496-4bad-a324-ba58da7ab1b0"), new DateTimeOffset(new DateTime(1995, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 10, 0, 0, 0)), "Bill", new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 814, DateTimeKind.Unspecified).AddTicks(4261), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("a164c1aa-f822-4217-b9ad-fae3ec211812"), new DateTimeOffset(new DateTime(2000, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 10, 0, 0, 0)), "Jolene", new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 814, DateTimeKind.Unspecified).AddTicks(4264), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Flavours",
                columns: new[] { "Id", "Name", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("79baab7f-19e7-499d-b05d-cda2f1f24360"), "Milk Tea", new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 816, DateTimeKind.Unspecified).AddTicks(2001), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("944ee5e2-12d6-4289-89bd-da9a096c57fc"), "Lemon", new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 816, DateTimeKind.Unspecified).AddTicks(1953), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("fa2a980d-9d50-49ff-bd0a-97be683353fd"), "PasisonFruit", new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 816, DateTimeKind.Unspecified).AddTicks(1998), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Sizes",
                columns: new[] { "Id", "Name", "PriceModifier", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("84b61fc0-7f77-41a2-ba31-31b33c3136f4"), "Small", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 823, DateTimeKind.Unspecified).AddTicks(1216), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("008339ab-bf9a-4478-8bd5-516dd64e1d13"), "Medium", 1.25m, new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 823, DateTimeKind.Unspecified).AddTicks(1271), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("4ab51dab-d52e-4c89-a3bd-0ae33065b5d9"), "Large", 1.5m, new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 823, DateTimeKind.Unspecified).AddTicks(1275), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Toppings",
                columns: new[] { "Id", "Name", "Price", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("ddf4ca07-d281-4a82-8bee-d2785c213a30"), "Black Tea", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 824, DateTimeKind.Unspecified).AddTicks(7564), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("cdbcbbdd-7b92-4f90-af85-3de8bf335cb6"), "Green Tea", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 824, DateTimeKind.Unspecified).AddTicks(7513), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("209c9e40-7a73-4ef8-ba02-5a6b7ce4204a"), "Milk Tea", 1.5m, new DateTimeOffset(new DateTime(2021, 1, 28, 23, 6, 25, 824, DateTimeKind.Unspecified).AddTicks(7568), new TimeSpan(0, 10, 0, 0, 0)) }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "BaseTeas",
                keyColumn: "Id",
                keyValue: new Guid("af8bde64-342c-4ada-aab4-67439b21ceb0"));

            migrationBuilder.DeleteData(
                table: "BaseTeas",
                keyColumn: "Id",
                keyValue: new Guid("e926435a-6520-4193-99ed-1f3b7b5bf369"));

            migrationBuilder.DeleteData(
                table: "BaseTeas",
                keyColumn: "Id",
                keyValue: new Guid("fdbfe3c7-4f22-49ff-ab3a-acb079848a5e"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("14d09ba5-aa7a-480c-9160-9b1991ef2cb7"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("a02e590e-936d-4a9e-9ba8-22dcff3c1773"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("a164c1aa-f822-4217-b9ad-fae3ec211812"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("a2420ceb-60ca-4277-9ea1-935cbd2343c9"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("fc7d3d15-8496-4bad-a324-ba58da7ab1b0"));

            migrationBuilder.DeleteData(
                table: "Flavours",
                keyColumn: "Id",
                keyValue: new Guid("79baab7f-19e7-499d-b05d-cda2f1f24360"));

            migrationBuilder.DeleteData(
                table: "Flavours",
                keyColumn: "Id",
                keyValue: new Guid("944ee5e2-12d6-4289-89bd-da9a096c57fc"));

            migrationBuilder.DeleteData(
                table: "Flavours",
                keyColumn: "Id",
                keyValue: new Guid("fa2a980d-9d50-49ff-bd0a-97be683353fd"));

            migrationBuilder.DeleteData(
                table: "Sizes",
                keyColumn: "Id",
                keyValue: new Guid("008339ab-bf9a-4478-8bd5-516dd64e1d13"));

            migrationBuilder.DeleteData(
                table: "Sizes",
                keyColumn: "Id",
                keyValue: new Guid("4ab51dab-d52e-4c89-a3bd-0ae33065b5d9"));

            migrationBuilder.DeleteData(
                table: "Sizes",
                keyColumn: "Id",
                keyValue: new Guid("84b61fc0-7f77-41a2-ba31-31b33c3136f4"));

            migrationBuilder.DeleteData(
                table: "Toppings",
                keyColumn: "Id",
                keyValue: new Guid("209c9e40-7a73-4ef8-ba02-5a6b7ce4204a"));

            migrationBuilder.DeleteData(
                table: "Toppings",
                keyColumn: "Id",
                keyValue: new Guid("cdbcbbdd-7b92-4f90-af85-3de8bf335cb6"));

            migrationBuilder.DeleteData(
                table: "Toppings",
                keyColumn: "Id",
                keyValue: new Guid("ddf4ca07-d281-4a82-8bee-d2785c213a30"));

            migrationBuilder.DropColumn(
                name: "DateOfBirth",
                table: "Customers");

            migrationBuilder.InsertData(
                table: "BaseTeas",
                columns: new[] { "Id", "Name", "Price", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("33c16b34-e218-4ba2-9e9f-32aa7d7f2734"), "Green Tea", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 680, DateTimeKind.Unspecified).AddTicks(9398), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("c448546d-d80b-4172-8ff2-4934ab4faff1"), "Black Tea", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 682, DateTimeKind.Unspecified).AddTicks(9060), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("9b81786e-4f68-4968-8690-3c2b845a45c2"), "Milk Tea", 1.5m, new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 682, DateTimeKind.Unspecified).AddTicks(9110), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Name", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("11eb8e8d-ad8c-4a17-afca-15c03a7e62cb"), "Scott", new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 686, DateTimeKind.Unspecified).AddTicks(3579), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("9433fc17-db68-46e1-936a-385e94a64c5f"), "Todd", new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 686, DateTimeKind.Unspecified).AddTicks(3618), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("e54f85c1-b5e5-4608-9a04-9628dad7ccef"), "Marsha", new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 686, DateTimeKind.Unspecified).AddTicks(3622), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("5f752355-8c4b-4936-a6f4-7e115961ad5f"), "Bill", new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 686, DateTimeKind.Unspecified).AddTicks(3625), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("a9ba7a06-8d23-44ff-8600-7639e1341fba"), "Jolene", new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 686, DateTimeKind.Unspecified).AddTicks(3628), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Flavours",
                columns: new[] { "Id", "Name", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("b6ea2c3e-83f3-4704-9b90-37f5fd6cc094"), "Milk Tea", new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 688, DateTimeKind.Unspecified).AddTicks(1813), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("73b53af0-7d7e-462a-818e-6ff738193e04"), "Lemon", new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 688, DateTimeKind.Unspecified).AddTicks(1766), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("2ac46f2b-4dda-4aa0-b3a0-448c9e52dc3e"), "PasisonFruit", new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 688, DateTimeKind.Unspecified).AddTicks(1809), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Sizes",
                columns: new[] { "Id", "Name", "PriceModifier", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("42f47479-f9da-40fe-87b8-795a425492d0"), "Small", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 695, DateTimeKind.Unspecified).AddTicks(5313), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("9e7534c1-fb6b-4b4b-843a-5cd254a37588"), "Medium", 1.25m, new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 695, DateTimeKind.Unspecified).AddTicks(5371), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("d68530d8-a6ac-4b09-af41-23226d7b5d3a"), "Large", 1.5m, new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 695, DateTimeKind.Unspecified).AddTicks(5376), new TimeSpan(0, 10, 0, 0, 0)) }
                });

            migrationBuilder.InsertData(
                table: "Toppings",
                columns: new[] { "Id", "Name", "Price", "Timestamp" },
                values: new object[,]
                {
                    { new Guid("05e821dd-5466-4d00-b3d6-31869a4566cd"), "Black Tea", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 697, DateTimeKind.Unspecified).AddTicks(1644), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("b2a3ed59-df77-4163-bba6-607f9f8edff1"), "Green Tea", 1m, new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 697, DateTimeKind.Unspecified).AddTicks(1599), new TimeSpan(0, 10, 0, 0, 0)) },
                    { new Guid("64b2b0f6-72bf-4f84-8784-47baa620e09f"), "Milk Tea", 1.5m, new DateTimeOffset(new DateTime(2021, 1, 28, 22, 12, 1, 697, DateTimeKind.Unspecified).AddTicks(1655), new TimeSpan(0, 10, 0, 0, 0)) }
                });
        }
    }
}
