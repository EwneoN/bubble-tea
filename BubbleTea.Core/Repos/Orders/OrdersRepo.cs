﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using BubbleTea.Core.Dtos;
using BubbleTea.Core.Models;
using BubbleTea.Core.Persistence;
using Microsoft.EntityFrameworkCore;

namespace BubbleTea.Core.Repos.Orders
{
	public class OrdersRepo : IOrdersRepo
	{
		private readonly BubbleTeaDbContext _DbContext;

		public OrdersRepo(BubbleTeaDbContext dbContext)
		{
			_DbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		}

		public async Task<Order> GetOrder(Guid id)
		{
			//this is not the real way to do this
			//in a real project i would select this into a DTO
			Order order = await _DbContext.Orders
				.Include(e => e.Customer)
				.Include(e => e.Size)
				.Include(e => e.BaseTea)
				.Include(e => e.Flavour)
				.Include(e => e.OrderToppings)
				.ThenInclude(e => e.Topping)
				.FirstOrDefaultAsync(e => e.Id == id);

			if (order == null)
			{
				//in a real project we would define a custom exception to throw
				throw new Exception($"No record found with an id of {id}");
			}

			return order;
		}

		public async Task<ICollection<Order>> GetOrders()
		{
			//this is not the real way to do this
			//in a real project i would select this into DTOs
			return await _DbContext.Orders
				.Include(e => e.Customer)
				.Include(e => e.Size)
				.Include(e => e.BaseTea)
				.Include(e => e.Flavour)
				.Include(e => e.OrderToppings)
				.ThenInclude(e => e.Topping)
				.ToListAsync();
		}

		public async Task<Order> CreateOrder(NewBubbleTeaOrder newOrder)
		{
			if (newOrder == null)
			{
				throw new ArgumentNullException(nameof(newOrder));
			}

			var transaction = await _DbContext.Database.BeginTransactionAsync();

			try
			{
				Order order = new Order
				{
					SizeId = newOrder.SizeId,
					BaseTeaId = newOrder.BaseTeaId,
					FlavourId = newOrder.FlavourId,
					CustomerId = newOrder.CustomerId
				};

				await _DbContext.Orders.AddAsync(order);
				await _DbContext.SaveChangesAsync();

				if (newOrder.ToppingIds != null && newOrder.ToppingIds.Length > 0)
				{
					foreach (Guid toppingId in newOrder.ToppingIds)
					{
						OrderTopping topping = new OrderTopping
						{
							Order = order,
							ToppingId = toppingId
						};

						await _DbContext.OrderToppings.AddAsync(topping);
						await _DbContext.SaveChangesAsync();
					}
				}

				await transaction.CommitAsync();

				// we call this method for the return value so we can include the related records
				// this has been done just for speed
				return await GetOrder(order.Id);
			}
			catch (Exception)
			{
				await transaction.RollbackAsync();
				throw;
			}
		}

		public async Task<Order> UpdateOrder(BubbleTeaOrderUpdate orderUpdate)
		{
			if (orderUpdate == null)
			{
				throw new ArgumentNullException(nameof(orderUpdate));
			}

			Order order = await _DbContext.Orders
				.Include(e => e.OrderToppings)
				.FirstOrDefaultAsync(e => e.Id == orderUpdate.Id);

			if (order == null)
			{
				//in a real project we would define a custom exception to throw
				throw new Exception($"No record found with an id of {orderUpdate.Id}");
			}

			order.SizeId = orderUpdate.SizeId;
			order.BaseTeaId = orderUpdate.BaseTeaId;
			order.FlavourId = orderUpdate.FlavourId;
			order.CustomerId = orderUpdate.CustomerId;

			if (orderUpdate.ToppingIds == null || orderUpdate.ToppingIds.Length <= 0)
			{
				_DbContext.OrderToppings.RemoveRange(order.OrderToppings);

				order.OrderToppings.Clear();
			}
			else
			{
				var removedRecords = order.OrderToppings
					.Where(e => !orderUpdate.ToppingIds.Contains(e.ToppingId))
					.ToArray();
				var newRecordIds = orderUpdate
					.ToppingIds
					.Where(i => !order.OrderToppings.Select(t => t.ToppingId).Contains(i))
					.ToArray();

				if (removedRecords.Length > 0)
				{
					foreach (var removedRecord in removedRecords)
					{
						order.OrderToppings.Remove(removedRecord);
					}

					_DbContext.OrderToppings.RemoveRange(removedRecords);
				}

				if (newRecordIds.Length > 0)
				{
					foreach (Guid toppingId in newRecordIds)
					{
						OrderTopping topping = new OrderTopping
						{
							Order = order,
							ToppingId = toppingId
						};
						
						await _DbContext.OrderToppings.AddAsync(topping);
						await _DbContext.SaveChangesAsync();
					}
				}
			}

			await _DbContext.SaveChangesAsync();

			// we call this method for the return value so we can include the related records
			// this has been done just for speed
			return await GetOrder(order.Id);
		}

		public async Task DeleteOrder(Guid id)
		{
			//this is not the real way to do this
			//in a real project i would select this into a DTO
			Order order = await _DbContext.Orders
				.Include(e => e.OrderToppings)
				.FirstOrDefaultAsync(e => e.Id == id);

			if (order == null)
			{
				//in a real project we would define a custom exception to throw
				throw new Exception($"No record found with an id of {id}");
			}

			if (!order.OrderToppings.Any())
			{
				_DbContext.OrderToppings.RemoveRange(order.OrderToppings);
				order.OrderToppings.Clear();
			}

			_DbContext.Orders.Remove(order);

			await _DbContext.SaveChangesAsync();
		}
	}
}