﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BubbleTea.Core.Dtos;
using BubbleTea.Core.Models;

namespace BubbleTea.Core.Repos.Orders
{
	public interface IOrdersRepo
	{
		Task<Order> GetOrder(Guid id);
		Task<ICollection<Order>> GetOrders();
		Task<Order> CreateOrder(NewBubbleTeaOrder newOrder);
		Task<Order> UpdateOrder(BubbleTeaOrderUpdate orderUpdate);
		Task DeleteOrder(Guid id);
	}
}
