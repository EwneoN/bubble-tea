﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BubbleTea.Core.Models;
using BubbleTea.Core.Persistence;
using Microsoft.EntityFrameworkCore;

namespace BubbleTea.Core.Repos.Sizes
{
	public class SizesRepo : ISizesRepo
	{
		private readonly BubbleTeaDbContext _DbContext;

		public SizesRepo(BubbleTeaDbContext dbContext)
		{
			_DbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		}

		public async Task<ICollection<Size>> GetSizes()
		{
			return await _DbContext.Sizes.ToListAsync();
		}
	}
}