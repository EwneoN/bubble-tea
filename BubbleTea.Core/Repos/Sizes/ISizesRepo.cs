﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BubbleTea.Core.Models;

namespace BubbleTea.Core.Repos.Sizes
{
	public interface ISizesRepo
	{
		Task<ICollection<Size>> GetSizes();
	}
}
