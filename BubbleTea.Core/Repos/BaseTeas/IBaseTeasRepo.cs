﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BubbleTea.Core.Dtos.BaseTeas;
using BubbleTea.Core.Models;

namespace BubbleTea.Core.Repos.BaseTeas
{
	public interface IBaseTeasRepo
	{
		Task<BaseTea> GetBaseTea(Guid id);
		Task<ICollection<BaseTea>> GetBaseTeas();
		Task<BaseTea> CreateBaseTea(NewBaseTea newBaseTea);
		Task<BaseTea> UpdateBaseTea(BaseTeaUpdate baseTeaUpdate);
		Task DeleteBaseTea(Guid id);
	}
}
