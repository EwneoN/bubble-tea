﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BubbleTea.Core.Dtos.BaseTeas;
using BubbleTea.Core.Models;
using BubbleTea.Core.Persistence;
using Microsoft.EntityFrameworkCore;

namespace BubbleTea.Core.Repos.BaseTeas
{
	public class BaseTeasRepo : IBaseTeasRepo
	{
		private readonly BubbleTeaDbContext _DbContext;

		public BaseTeasRepo(BubbleTeaDbContext dbContext)
		{
			_DbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		}

		public async Task<BaseTea> GetBaseTea(Guid id)
		{
			BaseTea baseTea = await _DbContext.BaseTeas.FirstOrDefaultAsync(e => e.Id == id);

			if (baseTea == null)
			{
				//in a real project we would define a custom exception to throw
				throw new Exception($"No record found with an id of {id}");
			}

			return baseTea;
		}

		public async Task<ICollection<BaseTea>> GetBaseTeas()
		{
			return await _DbContext.BaseTeas.ToListAsync();
		}

		public async Task<BaseTea> CreateBaseTea(NewBaseTea newBaseTea)
		{
			if (newBaseTea == null)
			{
				throw new ArgumentNullException(nameof(newBaseTea));
			}

			BaseTea baseTea = new BaseTea
			{
				Name = newBaseTea.Name,
				Price = newBaseTea.Price
			};

			await _DbContext.BaseTeas.AddAsync(baseTea);

			await _DbContext.SaveChangesAsync();

			return baseTea;
		}

		public async Task<BaseTea> UpdateBaseTea(BaseTeaUpdate baseTeaUpdate)
		{
			if (baseTeaUpdate == null)
			{
				throw new ArgumentNullException(nameof(baseTeaUpdate));
			}

			BaseTea baseTea = await _DbContext.BaseTeas.FirstOrDefaultAsync(e => e.Id == baseTeaUpdate.Id);

			if (baseTea == null)
			{
				//in a real project we would define a custom exception to throw
				throw new Exception($"No record found with an id of {baseTeaUpdate.Id}");
			}

			baseTea.Name = baseTeaUpdate.Name;
			baseTea.Price = baseTeaUpdate.Price;

			await _DbContext.SaveChangesAsync();

			return baseTea;
		}

		public async Task DeleteBaseTea(Guid id)
		{
			BaseTea baseTea = await _DbContext.BaseTeas
				.FirstOrDefaultAsync(e => e.Id == id);

			if (baseTea == null)
			{
				//in a real project we would define a custom exception to throw
				throw new Exception($"No record found with an id of {id}");
			}

			_DbContext.BaseTeas.Remove(baseTea);

			await _DbContext.SaveChangesAsync();
		}
	}
}