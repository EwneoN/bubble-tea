﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BubbleTea.Core.Dtos.Customers;
using BubbleTea.Core.Models;

namespace BubbleTea.Core.Repos.Customers
{
	public interface ICustomersRepo
	{
		Task<Customer> GetCustomer(Guid id);
		Task<ICollection<Customer>> GetCustomers();
		Task<Customer> CreateCustomer(NewCustomer newCustomer);
		Task<Customer> UpdateCustomer(CustomerUpdate customerUpdate);
		Task DeleteCustomer(Guid id);
	}
}
