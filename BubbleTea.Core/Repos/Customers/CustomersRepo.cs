﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BubbleTea.Core.Dtos.Customers;
using BubbleTea.Core.Models;
using BubbleTea.Core.Persistence;
using Microsoft.EntityFrameworkCore;

namespace BubbleTea.Core.Repos.Customers
{
	public class CustomersRepo : ICustomersRepo
	{
		private readonly BubbleTeaDbContext _DbContext;

		public CustomersRepo(BubbleTeaDbContext dbContext)
		{
			_DbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		}

		public async Task<Customer> GetCustomer(Guid id)
		{
			Customer customer = await _DbContext.Customers.FirstOrDefaultAsync(e => e.Id == id);

			if (customer == null)
			{
				//in a real project we would define a custom exception to throw
				throw new Exception($"No record found with an id of {id}");
			}

			return customer;
		}

		public async Task<ICollection<Customer>> GetCustomers()
		{
			return await _DbContext.Customers.ToListAsync();
		}

		public async Task<Customer> CreateCustomer(NewCustomer newCustomer)
		{
			if (newCustomer == null)
			{
				throw new ArgumentNullException(nameof(newCustomer));
			}

			Customer customer = new Customer
			{
				Name = newCustomer.Name,
				DateOfBirth = newCustomer.DateOfBirth
			};

			await _DbContext.Customers.AddAsync(customer);

			await _DbContext.SaveChangesAsync();

			return customer;
		}

		public async Task<Customer> UpdateCustomer(CustomerUpdate customerUpdate)
		{
			if (customerUpdate == null)
			{
				throw new ArgumentNullException(nameof(customerUpdate));
			}

			Customer customer = await _DbContext.Customers.FirstOrDefaultAsync(e => e.Id == customerUpdate.Id);

			if (customer == null)
			{
				//in a real project we would define a custom exception to throw
				throw new Exception($"No record found with an id of {customerUpdate.Id}");
			}

			customer.Name = customerUpdate.Name;
			customer.DateOfBirth = customerUpdate.DateOfBirth;

			await _DbContext.SaveChangesAsync();

			return customer;
		}

		public async Task DeleteCustomer(Guid id)
		{
			Customer customer = await _DbContext.Customers
				.FirstOrDefaultAsync(e => e.Id == id);

			if (customer == null)
			{
				//in a real project we would define a custom exception to throw
				throw new Exception($"No record found with an id of {id}");
			}

			_DbContext.Customers.Remove(customer);

			await _DbContext.SaveChangesAsync();
		}
	}
}