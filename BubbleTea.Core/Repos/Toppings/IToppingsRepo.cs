﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BubbleTea.Core.Dtos.Toppings;
using BubbleTea.Core.Models;

namespace BubbleTea.Core.Repos.Toppings
{
	public interface IToppingsRepo
	{
		Task<Topping> GetTopping(Guid id);
		Task<ICollection<Topping>> GetToppings();
		Task<Topping> CreateTopping(NewTopping newTopping);
		Task<Topping> UpdateTopping(ToppingUpdate toppingUpdate);
		Task DeleteTopping(Guid id);
	}
}
