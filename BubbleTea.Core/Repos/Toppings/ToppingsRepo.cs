﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BubbleTea.Core.Dtos.Toppings;
using BubbleTea.Core.Models;
using BubbleTea.Core.Persistence;
using Microsoft.EntityFrameworkCore;

namespace BubbleTea.Core.Repos.Toppings
{
	public class ToppingsRepo : IToppingsRepo
	{
		private readonly BubbleTeaDbContext _DbContext;

		public ToppingsRepo(BubbleTeaDbContext dbContext)
		{
			_DbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		}

		public async Task<Topping> GetTopping(Guid id)
		{
			Topping topping = await _DbContext.Toppings.FirstOrDefaultAsync(e => e.Id == id);

			if (topping == null)
			{
				//in a real project we would define a custom exception to throw
				throw new Exception($"No record found with an id of {id}");
			}

			return topping;
		}

		public async Task<ICollection<Topping>> GetToppings()
		{
			return await _DbContext.Toppings.ToListAsync();
		}

		public async Task<Topping> CreateTopping(NewTopping newTopping)
		{
			if (newTopping == null)
			{
				throw new ArgumentNullException(nameof(newTopping));
			}

			Topping topping = new Topping
			{
				Name = newTopping.Name
			};

			await _DbContext.Toppings.AddAsync(topping);

			await _DbContext.SaveChangesAsync();

			return topping;
		}

		public async Task<Topping> UpdateTopping(ToppingUpdate toppingUpdate)
		{
			if (toppingUpdate == null)
			{
				throw new ArgumentNullException(nameof(toppingUpdate));
			}

			Topping topping = await _DbContext.Toppings.FirstOrDefaultAsync(e => e.Id == toppingUpdate.Id);

			if (topping == null)
			{
				//in a real project we would define a custom exception to throw
				throw new Exception($"No record found with an id of {toppingUpdate.Id}");
			}

			topping.Name = toppingUpdate.Name;
			topping.Price = toppingUpdate.Price;

			await _DbContext.SaveChangesAsync();

			return topping;
		}

		public async Task DeleteTopping(Guid id)
		{
			Topping topping = await _DbContext.Toppings
				.FirstOrDefaultAsync(e => e.Id == id);

			if (topping == null)
			{
				//in a real project we would define a custom exception to throw
				throw new Exception($"No record found with an id of {id}");
			}

			_DbContext.Toppings.Remove(topping);

			await _DbContext.SaveChangesAsync();
		}
	}
}