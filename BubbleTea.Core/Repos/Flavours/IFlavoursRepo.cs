﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BubbleTea.Core.Dtos.Flavours;
using BubbleTea.Core.Models;

namespace BubbleTea.Core.Repos.Flavours
{
	public interface IFlavoursRepo
	{
		Task<Flavour> GetFlavour(Guid id);
		Task<ICollection<Flavour>> GetFlavours();
		Task<Flavour> CreateFlavour(NewFlavour newFlavour);
		Task<Flavour> UpdateFlavour(FlavourUpdate flavourUpdate);
		Task DeleteFlavour(Guid id);
	}
}
