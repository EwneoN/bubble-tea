﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BubbleTea.Core.Dtos.Flavours;
using BubbleTea.Core.Models;
using BubbleTea.Core.Persistence;
using Microsoft.EntityFrameworkCore;

namespace BubbleTea.Core.Repos.Flavours
{
	public class FlavoursRepo : IFlavoursRepo
	{
		private readonly BubbleTeaDbContext _DbContext;

		public FlavoursRepo(BubbleTeaDbContext dbContext)
		{
			_DbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		}

		public async Task<Flavour> GetFlavour(Guid id)
		{
			Flavour flavour = await _DbContext.Flavours.FirstOrDefaultAsync(e => e.Id == id);

			if (flavour == null)
			{
				//in a real project we would define a custom exception to throw
				throw new Exception($"No record found with an id of {id}");
			}

			return flavour;
		}

		public async Task<ICollection<Flavour>> GetFlavours()
		{
			return await _DbContext.Flavours.ToListAsync();
		}

		public async Task<Flavour> CreateFlavour(NewFlavour newFlavour)
		{
			if (newFlavour == null)
			{
				throw new ArgumentNullException(nameof(newFlavour));
			}

			Flavour flavour = new Flavour
			{
				Name = newFlavour.Name
			};

			await _DbContext.Flavours.AddAsync(flavour);

			await _DbContext.SaveChangesAsync();

			return flavour;
		}

		public async Task<Flavour> UpdateFlavour(FlavourUpdate flavourUpdate)
		{
			if (flavourUpdate == null)
			{
				throw new ArgumentNullException(nameof(flavourUpdate));
			}

			Flavour flavour = await _DbContext.Flavours.FirstOrDefaultAsync(e => e.Id == flavourUpdate.Id);

			if (flavour == null)
			{
				//in a real project we would define a custom exception to throw
				throw new Exception($"No record found with an id of {flavourUpdate.Id}");
			}

			flavour.Name = flavourUpdate.Name;

			await _DbContext.SaveChangesAsync();

			return flavour;
		}

		public async Task DeleteFlavour(Guid id)
		{
			Flavour flavour = await _DbContext.Flavours
				.FirstOrDefaultAsync(e => e.Id == id);

			if (flavour == null)
			{
				//in a real project we would define a custom exception to throw
				throw new Exception($"No record found with an id of {id}");
			}

			_DbContext.Flavours.Remove(flavour);

			await _DbContext.SaveChangesAsync();
		}
	}
}